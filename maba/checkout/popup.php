<!-- BEGIN: Pop Up -->
<table id="signup_settings" class="popup <?php echo $show ? 'show' : ''; ?>">
	<tr>
		<td>
			<div class="left_side">

				<div class="headline futura-condensed">
					Register Now!<br /><br />

				</div>

				<div class="white-border"></div>

				<img src="images/guarantee.png" width="200" height="200" alt="2x Money Back Guarantee" />

				<p class="guarantee">
					If you decide during MABS that the info is not cutting
					edge and will not help you with your business then let
					our support staff know immediately and we'll give you
					a 200% refund on the spot.
				</p>

			</div>
		</td>

		<td>
			<div class="right_side">

				<div class="close">X</div>


				<div class="section futura-condensed" style="text-indent: 30px; font-size: 24px; text-transform: uppercase; margin-bottom: 17px; padding-top: 30px;">1. Do you want The EZ Payment Option?</div>

				<p class="text">
					The EZ Option lets you pay for your ticket in multiple payments. Each payment is 30 days apart.
				</p>

				<select name="ez_payment">
					<option value="0">I Prefer One Payment of $<?php echo $ticket_info['cost']; ?>  (A $192 savings!)</option>

                    <?php
						if(is_array($ez_info['id'])):
							foreach($ez_info as $ez):
					?>
							<option value="<?php echo $ez['id']; ?>">Yes, I want the EZ Option (<?php echo $ez['num']; ?> Payments of $<?php echo $ez['cost']; ?>)</option>
                    <?php
							endforeach;
						elseif(is_array($ez_info)):
					?>
						<option value="<?php echo $ez_info['id']; ?>">Yes, I want the EZ Option (<?php echo $ez_info['num']; ?> Payments of $<?php echo $ez_info['cost']; ?>)</option>
					<?php endif; ?>

				</select>



            <a href="index.php">
				<input type="button" id="" class="checkout" value="Register" style="margin-bottom: 10px; float: none; display: block; margin: auto; margin-top: 40px;"/>
            </a>

			</div>
		</td>
	</tr>
</table>
<!-- END: Pop Up -->