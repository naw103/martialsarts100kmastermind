<?php
	require('program/definitions.php');
	require('program/class.database.php');
	require('program/class.reports.php');

	$db = new Reports(DB_HOST, DB_USER, DB_PASS, DB_NAME);
	$db->open();

	$total_signups = $db->get_total_signups();

	$total_signups_num = sizeof($total_signups);

	//$db->get_event_signups();

	$db->close();
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8" />
<title>1-Day Private Mastermind with Mike Parrella</title>

<link rel="stylesheet" href="https://www.ilovekickboxing.com/intl_css/reset.css"/>
<link rel="stylesheet" href="css/pages.css"/>
<link rel="stylesheet" href="css/styles.css"/>


<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" href="css/datepicker.css">


 <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!--[if lt IE 9]>
  <script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->



</head>
<body>
<div id="coupon_container">

	<label for="coupon_name">Name:</label>
	<input type="text" name="coupon_name" value="" /><br />

	<label for="coupon_code">Code:</label>
	<input type="text" name="coupon_code" value="" /><br />

	<label for="coupon_type">Type:</label>
	<select name="coupon_type">
		<option value="">Select</option>
		<option value="$">$</option>
		<option value="%">%</option>
	</select><br />

	<label for="coupon_amt">Amount:</label>
	<input type="text" name="coupon_amt" value="" /><br />

	<label for="coupon_start">Start Date:</label>
	<input type="text" class="datepicker" name="coupon_start" value="" /><br />

	<label for="coupon_end">End Date:</label>
	<input type="text" class="datepicker" name="coupon_end" value="" /><br />

	<label for="coupon_active">Active:</label>
	<select name="coupon_active">
		<option value="">Select</option>
		<option value="0">No</option>
		<option value="1">Yes</option>
	</select><br />

	<input type="button" id="btn_coupon_submit" value="Save Coupon" />

</div>
<!--
<header>
	<div class="container">
		<h1>1 Day MasterMind Admin</h1>
	</div>
</header>

<div class="container">

	<div id="highlite_row">

		<div id="total_li" class="highlite">
			<div class="title">Long Island</div>
			<div class="content"><?php echo $total_signups_num; ?></div>
		</div>

		<div id="total_sandiego" class="highlite">
			<div class="title">San Diego</div>
			<div class="content"><?php echo $total_signups_num; ?></div>
		</div>

		<div id="total_neworleans" class="highlite">
			<div class="title">New Orleans</div>
			<div class="content"><?php echo $total_signups_num; ?></div>
		</div>

		<div id="total" class="highlite">
			<div class="title">Total</div>
			<div class="content"><?php echo $total_signups_num; ?></div>
		</div>

	</div>

<div class="wrapper">
		<h2>All Signups</h2>
		<table id="total_signups" class="signups">
			<thead>
				<td>First Name</td>
				<td>Last Name</td>
				<td>When/Where</td>
				<td>Order Date</td>
				<td></td>
				<td>Order Total</td>
			</thead>

			<tbody>
				<?php for($i = 0; $i < sizeof($total_signups); $i++): ?>
				<tr>
					<td><?php echo $total_signups[$i]['firstname']; ?></td>
					<td><?php echo $total_signups[$i]['lastname']; ?></td>
					<td><?php echo $total_signups[$i]['mm_text']; ?></td>
					<td><?php echo date('m/d/y', strtotime($total_signups[$i]['order_date'])); ?></td>
					<td><?php echo $total_signups[$i]['order_date']; ?></td>
					<td><?php echo '$' . $total_signups[$i]['order_total']; ?></td>
				</tr>
				<?php endfor; ?>
			</tbody>
		</table>
	</div>
</div>
-->
<script src="js/jquery.dataTables.js"></script>
<script>
$(document).ready(function(){

		 var oTable = $('table').dataTable({
			"aaSorting": [[ 4, "desc" ]],
			    "aoColumnDefs": [

			{ "bVisible": false,  "aTargets": [ 4 ] }
		]

		});


});

</script>
<script src="js/process_coupon.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

<script>
$(function() {
	$( ".datepicker" ).datepicker();
});
</script>

</body>
</html>