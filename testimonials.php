<?php include("program/program.php"); ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8" />
<title>Six-Figure Martial Arts MasterMind</title>

<meta name="Description" content=""/>

<meta name="keywords" content=""/>


<link rel="stylesheet" href="https://www.ilovekickboxing.com/intl_css/reset.css"/>
<link rel="stylesheet" href="css/pages.css"/>
<link rel="stylesheet" href="css/testimonials.css"/>


<script src="https://www.ilovekickboxing.com/intl_js/jquery.js"></script>
<!--[if lt IE 9]>
  <script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-29420623-21', 'martialarts100kmastermind.com');
  ga('send', 'pageview');

</script>


</head>
<body>
<?php include("header.php"); ?>

<div class="container">

	<!-- BEGIN: Page Content -->
	<div id="page_content">
		<?php include('navigation.php'); ?>

		<div id="headline-container">

			<h1>
        		Mike Parrella's 6-Figure MasterMind. For School Owners Ready<br>
				For Rockstar  Numbers... While Working Way Less Hours.
			</h1>

			<h2 class="futura">
				<strong>"Don't just take my word for it.<br>
				Check out what these guys and<br>
				gals had to say for themselves."</strong>
			</h2>

		</div>

		<div id="testimonial-container">

			<article class="testimonial">

				<img class="headshot" src="images/testimonials/1-1.png" alt="" />
				<img class="headshot" src="images/testimonials/1-2.png" alt="" />

				<p class="futura head">
                	"Our business just about doubled within 2 months of working with Mike.
					And to make all of this extra money, we now only work a total of
					4-6 hours a week."
				</p>
				<p>
                    Before joining the MasterMind our school was already rocking it.
					<strong>However, we had absolutely NO time to enjoy it.</strong> On top of
					that, we have two kids who we wanted to devote more time to (a 2 year
					old and a 4 year old).
				</p>
				<p>
					In fact, before our kids were born, Cara worked in the school right up
					until the day before she gave birth. That's how bad it was. <strong>We
					were working 6 days a week, and easily 60+ hours inside the school.</strong>
					And both of us absolutely had to be there. If one of us got sick... It
					wasn't pretty.
				</p>
				<p>
					Mike basically said to me, "Why are you making things so damn hard on
					yourself?" He was blunt, but I could tell he cared. He then told us,
					"I can show you how to do everything you're doing now WAY easier, make
					more money, and have WAY more free time so you can actually enjoy
					your lives."
				</p>
				<p>
					<strong>Today, I work 4 hours a week inside the school. Cara works 6 hours.</strong>
					We have all the time in the world for our children, our school is
					helping more people than ever with our programs, and life just
					feels good.
				</p>
				<p>
					<strong>We're both extremely grateful for MasterMind, Mike Parrella & Ryan
					Healy.</strong> They've changed our lives in the most incredible ways.
				</p>
				<p>
                	I don't know any other way to say this than to just say it: Man up
					(or woman up), make the investment, and do it. <strong>Empty your "cup"
					and just follow what Mike and Ryan have to say. It's one of the best
					decisions we've ever made.</strong>
				</p>
				<p class="daniel sig">Chuck & Kara Giangreco, NY</p>

			</article>

            <article class="testimonial">

				<img class="headshot" src="images/testimonials/2.png" alt="" />

				<p class="futura head">
                	"Thanks to Mike Parrella, my billing check is now over $32,000 each month."
				</p>

				<p class="daniel sig">David Ross, NY</p>

			</article>

			<article class="testimonial">

				<img class="headshot" src="images/testimonials/3.png" alt="" />

				<p class="futura head">
                	"I went from not even taking a paycheck from my school to having
					2 locations that gross over $75k/month."
				</p>
				<p>
                    I started working with Michael Parella three years ago. At the time,
					I had a single location with two staff members, barely grossing
					12K/month. I was not taking a paycheck.
				</p>
				<p>
					Currently, I have two locations and seven staff members grossing
					50K/month and growing
				</p>
				<p>
					<strong>I used to work six days, 60-70 hours per week at that one
					location</strong>-I was responsible for everything. <strong>Now,
					between the two locations, I work 3-5 hours a day on site,</strong>
					reserving the rest of my work week to grow the business and spend
					quality time doing things I love.
				</p>
				<p class="daniel sig">Walter Rowe, RI</p>

			</article>

			<article class="testimonial no_horiz">

				<img class="headshot" src="images/testimonials/9.png" alt="" />

				<p class="futura head">
                	"I went from making 30k / month to 52k / month in less than a year."
				</p>

				<p class="daniel sig">Pedro Xavier, MA</p>

			</article>

			<a href="<?php echo $url_apply_now; ?>">
        		<img class="apply_now" src="images/index/button-apply-now.png" alt="Apply Now" />
			</a>


			<article class="testimonial">

				<img class="headshot" src="images/testimonials/4.png" alt="" />

				<p class="futura head">
                	"I can have dinner with my family whenever I want now. And my
					monthly billing went from $25k to now doing $40k consistently"
				</p>
				<p>
                    <strong>My experience with Mike and his master mind group has been
					a life changer.</strong>
				</p>
				<p>
					Working with the Mike and the Mastermind has created more free time
					to work on the parts of my personal life and business life that were
					causing me frustration because I felt like they were not getting the
					attention they deserved.
				</p>
				<p>
					First and foremost I can have dinner with my family whenever I want
					now.  That is rare in our industry because our business hours are
					typically during that family time after school. <strong>I can go to
					my children's games, concerts and other activities and not be worried
					about what is going on in my business.</strong>
				</p>
				<p>
					Mike's advice on how to pull myself out of my business was a big
					breakthrough for me. Being out of the daily activity has <strong>allowed
					me to pursue other interests and ideas that I have been wanting to do
					for years.</strong> Having more free time has put us in a position to
					realistically expand our business at a faster and more sustainable
					rate of speed than we ever thought we could in the past.  This has
					also allowed me to break through the occasional feeling of "burnout"
					that can happen when I wasn't always feeling like I was moving forward
					or making progress with my business.
				</p>
				<p>
					I would highly recommend Mike's Mastermind group to anyone who wants
					to take themselves to and their business to an entirely new level of
					success and sustainability.
				</p>
				<p class="daniel sig">Brett Lechtenberg, UT</p>

			</article>

			<article class="testimonial">

				<img class="headshot" src="images/testimonials/7.png" alt="" />

				<p class="futura head">
                	"I now have 3 locations, yet I work fewer hours than I've ever
					worked in my entire life."
				</p>

				<p class="daniel sig">David Inman, NV</p>

			</article>

			<article class="testimonial">

				<img class="headshot" src="images/testimonials/5.png" alt="" />

				<p class="futura head">
                	"From $35k / month to $82k / month."
				</p>
				<p>
					Mike's MasterMind Group has been a game changer for my business. He
					helped me change my mindset from operating as a regular martial arts
					school owner to operating as a successful business owner.
				</p>
				<p>
					Before the MasterMind I was constantly majoring in minor things that
					brought no profitability or results to my location. Now I only focus
					on my 5% which are things that can provide results for my business
					and opportunities for my staff.
				</p>
				<p>
					Since the time we started we have experienced a change in the way we
					see and treat our students and staff. My business has also grown by
					47% and Thank God continues to grow. We are proud and thankful to be
					part of such an awesome group.
				</p>
				<p class="daniel sig">Larry Batista, NY</p>

			</article>

			<article class="testimonial">

				<img class="headshot" src="images/testimonials/10.png" alt="" />

				<p class="futura head">
                	"Monthly billing went from $22,000 to $35,000 since working with Michael."
				</p>

				<p class="daniel sig">Alexandria Buzzell, MA</p>

			</article>

			<article class="testimonial no_horiz">

				<img class="headshot" src="images/testimonials/6.png" alt="" />

				<p class="futura head">
                	"I tried every 'expert' out there. But it was Mike Parrella who
					brought my billing from $3,000 / month to now $18,000 / month and rising."
				</p>
				<p>
					During my 10 years in business, I tried a number of programs by "experts"
					who promised amazing income and growth in exchange for their fee. What
					I got every time was simply more work to do with no guidance and no
					results to speak of.  So when I found Mike Parrella's programs I was
					quite a skeptic.
				</p>
				<p>
					My monthly income then was about $1300 / month. Before joining his
					MasterMind, I implemented some of his systems and it rose to $3,000 /
					month.  <strong>Then I joined the MasterMind, and as a result of his
					continued guidance, my business currently collects $12,500 per month,
					and there is no end in sight for our growth.</strong>
				</p>
				<p>
					Michael told me early on that it would be easy to get my business
					billing $20,000 per month, which is a milestone I'm quickly nearing.
				</p>
				<p>
                	<strong>Michael is always available when I need more of his professional
					help, which I truly appreciate.</strong>  He is the first and only
					business professional I have met who does what he says and delivers
					what he promises.
				</p>
				<p class="daniel sig">David Meyer, MN</p>

			</article>

			<a href="<?php echo $url_apply_now; ?>">
        		<img class="apply_now" src="images/index/button-apply-now.png" alt="Apply Now" />
			</a>

			<article class="testimonial">

				<img class="headshot" src="images/testimonials/11.png" alt="" />

				<p class="futura head">
                	"I started the Mastermind making $24,000 / month. I now earn
					$36,000 / month while traveling the world and lovin' life."
				</p>

				<p class="daniel sig">Patrick Consing, NY</p>

			</article>



			<article class="testimonial no_horiz">

				<img class="headshot" src="images/testimonials/8-1.png" alt="" />
				<img class="headshot" src="images/testimonials/8-2.png" alt="" />

				<p class="futura head">
                	"Monthly billing went from $2,000 to $15,000 in 60 days."
				</p>
				<p>
					We used to have 14 hour days. Now, we can actually have a life. We
					brought our kids into the business too, and we're leaving them a
					better business for their future.
				</p>
				<p>
                	From one promotion Michael taught us, In 3 days we had hundreds of
					people in our doors who we'd never seen before in our lives. Michael
					has it all down to a science.
				</p>

				<p class="daniel sig">Al & Nicole Agon, FL</p>

			</article>


			<a href="<?php echo $url_apply_now; ?>">
        		<img class="apply_now" src="images/index/button-apply-now.png" alt="Apply Now" />
			</a>

		</div>


	</div>
	<!-- END: Page Content -->

</div>

<?php include("footer.php"); ?>
</body>
</html>