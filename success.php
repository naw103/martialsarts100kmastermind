<?php include("program/program.php"); ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8" />
<title>Six-Figure Martial Arts MasterMind</title>

<meta name="Description" content=""/>

<meta name="keywords" content=""/>


<link rel="stylesheet" href="https://www.ilovekickboxing.com/intl_css/reset.css"/>
<link rel="stylesheet" href="css/pages.css"/>
<link rel="stylesheet" href="css/bio.css"/>


<script src="https://www.ilovekickboxing.com/intl_js/jquery.js"></script>
<!--[if lt IE 9]>
  <script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-29420623-21', 'martialarts100kmastermind.com');
  ga('send', 'pageview');

</script>


</head>
<body>
<?php include("header.php"); ?>

<div class="container">

	<!-- BEGIN: Page Content -->
	<div id="page_content">
		<?php include('navigation.php'); ?>

		<div id="headline-container" style="height: 500px;">

			<h1>
        		Mike Parrella's 6-Figure MasterMind. For School Owners Ready<br>
				For Rockstar  Numbers... While Working Way Less Hours.
			</h1>

			<h2 class="futura"><strong>Thank You!</strong></h2>

			<p style="font-size: 17px; text-align: center; margin-top: 30px;">
				We will review your application and be in contact with you shortly.
			</p>

		</div>


	</div>
	<!-- END: Page Content -->

</div>

<?php include("footer.php"); ?>
</body>
</html>