<?php
	include("program/program.php");
	$state = getStates();

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8" />
<title>Six-Figure Martial Arts MasterMind</title>

<meta name="Description" content=""/>

<meta name="keywords" content=""/>


<link rel="stylesheet" href="https://www.ilovekickboxing.com/intl_css/reset.css"/>
<link rel="stylesheet" href="css/pages.css"/>
<link rel="stylesheet" href="css/signup.css"/>


<script src="https://www.ilovekickboxing.com/intl_js/jquery.js"></script>
<!--[if lt IE 9]>
  <script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-29420623-21', 'martialarts100kmastermind.com');
  ga('send', 'pageview');

</script>

</head>
<body>
<?php include("header.php"); ?>

<div class="container">

	<!-- BEGIN: Page Content -->
	<div id="page_content">
		<?php include('navigation.php'); ?>

		<div id="headline-container">

			<h1>
        		Mike Parrella's 6-Figure MasterMind. For School Owners Ready<br>
				For Rockstar  Numbers... While Working Way Less Hours.
			</h1>

		</div>

		<div class="copy">

			<p class="title">What you get:</p>

			<ul>
			<!--
				<li>Four 2-day 'Mastermind' meetings facilitated by Michael Parrella</li>
				<li>Some meetings may have BONUS guests and other top marketing experts</li>
				<li>Two EMERGENCY one-on-one 15 minute coaching calls with Michael</li>
				<li>
					Two VIP registrations for the next annual Martial Arts Mastermind Summit&reg;
					for primary member and partner, spouse or key employee ($697.00 Value)
				</li>
				<li>Access to bonus workshops, meetings, and events hosted by Michael</li>
				<li>Qualify to win a Land Rover for one year at Martial Arts Mastermind Summit 2014</li>
				<li>Access (involvement) to all the fun, shenanigans, and good times that may take place.</li>
			-->
			<li>Three 2-day 'Mastermind' meetings facilitated by Team Parrella</li>
			<li>Some meetings may have BONUS guests and other top marketing experts</li>
			<li>Two EMERGENCY one-on-one 15 minute coaching calls with a Team Parrella member</li>
			<li>Two VIP registrations for the next annual Martial Arts Business Summit® for primary member and partner, spouse or key employee ($697.00 Value)</li>
			<li>Access to bonus workshops, meetings, and events hosted by Michael</li>
			<li>Qualify to win exclusive "Mastermind Only" Martial Arts Mastermind Summit 2016</li>
			<li>Access (involvement) to all the fun, shenanigans, and good times that may take place.</li>

			</ul>

			<p class="title">Membership Criteria:</p>

			<ol>
			<!--
                 <li>Be willing to share during the meetings about your successes, victories, failures, frustrations and problems</li>
				<li>Be willing to present your greatest successes at Martial Arts Mastermind Summit (if asked)</li>
				<li>Attend 'at least' three of the four yearly meetings (we're only looking for SERIOUS action takers)</li>
				<li>
					Have an honest desire to create a successful Martial Arts/Fitness
					business where clients can get world-class service and results
				</li>
				<li>
					Maintain confidentiality of all sensitive information discussed at
					the mastermind meetings
				</li>
				<li>Twelve-month commitment</li>
				<li>Qualify</li>
				-->
				<li>Be willing to share during the meetings about your successes, victories, failures, frustrations and problems</li>
				<li>Be willing to present your greatest successes at Martial Arts Business Summit (if asked)</li>
				<li>Attend 'at least' two of the three yearly meetings (we're only looking for SERIOUS action takers)</li>
				<li>Have an honest desire to create a successful Martial Arts/Fitness business where clients can get world-class service and results</li>
				<li>Maintain confidentiality of all sensitive information discussed at the mastermind meetings</li>
				<li>Twelve-month commitment</li>
				<li>Qualify</li>
			</ol>

		</div>


		<!-- BEGIN: Apply Now Form -->
		<div id="form-wrapper">

			<h4 class="futura">Mastermind Qualification Application</h4>

			<form name="apply-now" method="post" action="program/process_form.php">

				<label for="business_description">
					Descriptions of your business (How long in business, how many members,
					how much monthly gross revenue do you bring in annually)
				</label>
				<textarea name="business_description" rows="7" cols="82" /></textarea><br />

				<label for="business_obstacles">
					What obstacles and challenges have you encountered in life and business
					and how did you overcome them?
				</label>
				<textarea name="business_obstacles" rows="7" cols="82" /></textarea><br />

				<label for="business_challenges">
					What do you feel are the top challenges you're having in your business?
				</label>
				<textarea name="business_challenges" rows="7" cols="82" /></textarea><br />

				<label for="business_candidate">
					Briefly describe why you feel you're a good candidate for the "6
					Figure Mastermind" Coaching Program:
				</label>
				<textarea name="business_candidate" rows="7" cols="82" /></textarea><br />

				<label for="business_goals">
					What are your business goals for the next 12 months?
				</label>
				<textarea name="business_goals" rows="7" cols="82" /></textarea><br />


				<div class="form_group">

					<p class="title">
						<strong>CONFIDENTIAL data</strong><br />
						Business Gross Volume in...
					</p>

					<div class="inner_wrapper">
						<label for="year_2012"><?php echo (date("Y") - 1);?></label>
						<input type="radio" name="year_2012" value="Under $100,000" />&nbsp;&nbsp;Under $100,000<br />
						<input type="radio" name="year_2012" value="$100,000,01 - $250,000" />&nbsp;&nbsp;$100,000.01 - $250,000<br />
						<input type="radio" name="year_2012" value="$250,000,01 - $500,000.00" />&nbsp;&nbsp;$250,000.01 - $500,000.00<br />
						<input type="radio" name="year_2012" value="$500,001 - $999,999" />&nbsp;&nbsp;$500,001 - $999,999<br />
						<input type="radio" name="year_2012" value="1 Million +" />&nbsp;&nbsp;$1 Million +<br />
	                </div>

					<div class="inner_wrapper">
						<label for="year_2011"><?php echo (date("Y") - 2);?></label>
						<input type="radio" name="year_2011" value="Under $100,000" />&nbsp;&nbsp;Under $100,000<br />
						<input type="radio" name="year_2011" value="$100,000,01 - $250,000" />&nbsp;&nbsp;$100,000.01 - $250,000<br />
						<input type="radio" name="year_2011" value="$250,000,01 - $500,000.00" />&nbsp;&nbsp;$250,000.01 - $500,000.00<br />
						<input type="radio" name="year_2011" value="$500,001 - $999,999" />&nbsp;&nbsp;$500,001 - $999,999<br />
						<input type="radio" name="year_2011" value="1 Million +" />&nbsp;&nbsp;$1 Million +<br />
	                </div>

					<div class="inner_wrapper">
						<label for="year_2010"><?php echo (date("Y") - 3);?></label>
						<input type="radio" name="year_2010" value="Under $100,000" />&nbsp;&nbsp;Under $100,000<br />
						<input type="radio" name="year_2010" value="$100,000,01 - $250,000" />&nbsp;&nbsp;$100,000.01 - $250,000<br />
						<input type="radio" name="year_2010" value="$250,000,01 - $500,000.00" />&nbsp;&nbsp;$250,000.01 - $500,000.00<br />
						<input type="radio" name="year_2010" value="$500,001 - $999,999" />&nbsp;&nbsp;$500,001 - $999,999<br />
						<input type="radio" name="year_2010" value="1 Million +" />&nbsp;&nbsp;$1 Million +<br />
	                </div>

				 </div>

				 <div class="form_group rating">

					 <p class="title">
					 	What areas do you/your business needs the most work on?<br />
						(Rank EACH 1-10, 10 "Huge", 1 "Not significant")
					</p>

					<table>
						<tr style="color: red;">
							<td>Example: Clarity Of Goals &amp; Objectives</td>
							<td><div style="text-align: center; background: #F8F8F8;">9</div></td>
                    	</tr>

						<tr>
							<td>Clarity Of Goals &amp; Objectives</td>
							<td><div><input type="text" name="rating_clarity" class="rating" value="" /></div></td>
                    	</tr>

						<tr>
							<td>Time Management & Personal Productivity</td>
							<td><div><input type="text" name="rating_time_mangement" class="rating" value="" /></div></td>
						</tr>

						<tr>
							<td>Handling Employees/Outsourcing/Vendors</td>
							<td><div><input type="text" name="rating_outsourcing" class="rating" value="" /></div></td>
                    	</tr>

						<tr>
							<td>Raising Prices</td>
							<td><div><input type="text" name="rating_prices" class="rating" value="" /></div></td>
                    	</tr>

						<tr>
							<td>Customer Service</td>
							<td><div><input type="text" name="rating_customer_service" class="rating" value="" /></div></td>
                    	</tr>

						<tr>
							<td>Dealing with increased competition</td>
							<td><div><input type="text" name="rating_competition" class="rating" value="" /></div></td>
                    	</tr>

						<tr>
							<td>Copywriting</td>
							<td><div><input type="text" name="rating_copywriting" class="rating" value="" /></div></td>
                    	</tr>

						<tr>
							<td>Email Strategy</td>
							<td><div><input type="text" name="rating_email_strategy" class="rating" value="" /></div></td>
                     	</tr>

						<tr>
							<td>Program/Service development/creation</td>
							<td><div><input type="text" name="rating_creation" class="rating" value="" /></div></td>
                    	</tr>

						<tr>
							<td>Program launch/re-launch</td>
							<td><div><input type="text" name="rating_program_launch" class="rating" value="" /></div></td>
                    	</tr>

						<tr>
							<td>Traffic strategies</td>
							<td><div><input type="text" name="rating_traffic_strategies" class="rating" value="" /></div></td>
                     	</tr>

						<tr>
							<td>Tracking &amp; testing</td>
							<td><div><input type="text" name="rating_tracking_testing" class="rating" value="" /></div></td>
                    	</tr>

						<tr>
							<td>Conversion and multiple purchases</td>
							<td><div><input type="text" name="rating_conversion" class="rating" value="" /></div></td>
                    	</tr>

						<tr>
							<td>Affiliate and joint venture marketing</td>
							<td><div><input type="text" name="rating_affiliate" class="rating" value="" /></div></td>
                    	</tr>

						<tr>
							<td>Offline marketing</td>
							<td><div><input type="text" name="rating_offline_marketing" class="rating" value="" /></div></td>
                    	</tr>
					</table>

				 </div>

				 <div class="form_group rating">

					<p class="title">
						<strong>Self-Assessment:</strong><br>
						Rank 1 = Poor to 5 = Excellent
					</p>

					<table>
						<tr>
							<td>Advertising, Marketing Skills</td>
							<td><div><input type="text" name="rating_marketing" class="rating" value="" /></div></td>
						</tr>

						<tr>
							<td>Time Allocated To Marketing & Sales</td>
							<td><div><input type="text" name="rating_time_allocated" class="rating" value="" /></div></td>
                    	</tr>

						<tr>
							<td>Systematic, On Schedule Achievement Of Goals</td>
							<td><div><input type="text" name="rating_systematic" class="rating" value="" /></div></td>
                    	</tr>

						<tr>
							<td>Time Management & Personal Productivity</td>
							<td><div><input type="text" name="rating_personal_productivity" class="rating" value="" /></div></td>
                    	</tr>

						<tr>
							<td>Passion, Enthusiasm, Mind Set, Enjoyment Of Business Life</td>
							<td><div><input type="text" name="rating_passion" class="rating" value="" /></div></td>
                    	</tr>

						<tr>
							<td>Satisfaction With Income & Income Growth</td>
							<td><div><input type="text" name="rating_income_growth" class="rating" value="" /></div></td>
						</tr>

						<tr>
							<td>Quality Of Personal, Family Related Relationships</td>
							<td><div><input type="text" name="rating_relationships" class="rating" value="" /></div></td>
						</tr>

					</table>

				 </div>

				<label for="important_note">
					What's important for Michael to know about you personally?
				</label>
				<textarea name="important_note" rows="7" cols="82" /></textarea><br />


				<div class="form_group contact">
					<p class="title">
						All applications will be time stamped once received. And decisions<br />
						between equally qualified candidates will be based on who applied first.
					</p>

					<label for="fullname">Name:</label>
					<input class="med" type="text" name="fullname" />

					<label for="phone">Phone:</label>
					<input type="text" class="med" name="phone" /><br>

					<label for="address">Address:</label>
					<input type="text" class="med" name="address" />
					<input type="text" class="med" name="address1" /><br>

					<label for="city">City:</label>
					<input class="med" type="text" name="city" />

					<label for="statecode">State:</label>
					<select name="statecode">
						<option value="0">Please Select a State</option>
						<?php for($i = 0; $i < sizeof($state); $i++): ?>
						<option value="<?php echo $state[$i]['id']; ?>"><?php echo $state[$i]['name']; ?></option>
						<?php endfor; ?>
					</select>

					<label for="zipcode">Zip Code:</label>
					<input type="text" name="zipcode" style="width: 100px;" /><br>

					<label for="email">Email:</label>
					<input type="text" class="med" name="email" /><br>

				</div>

				<div class="form_group investment">
                	<p class="title" style="font-size: 22px;">Investment:</p>
<!--
					<p>
                        - $1,997.00 upon acceptance and $1,499/month for 11 months.<br />
						- Or pay $13,999 in full for the entire year and save $4,487
					</p>
-->
					<p>
						- $999.00 (regularly $2,500) upon acceptance and $1,199/month for 12 months.<br>
						- Or pay $13,389 in full for the entire year and save $1,998
					</p>

					<p>"If I qualify...I'll pay:" (Please Select an Option)</p>


                    <div class="wrapper">
					    <input type="radio" name="pay_option" value="Monthly payments" />&nbsp;&nbsp;
					    Monthly payments ($999 buy in and $1,199/month a 12 month commitment)<br />

					    <input type="radio" name="pay_option" value="Pre-pay" />&nbsp;&nbsp;
					    Pre-pay $13,389 for the year and take the $1,998 discount.<br /><br />

                    </div>

                    <!--<input type="checkbox" name="coaching_option" value="one-on-one coaching" />&nbsp;&nbsp;
					<span style="color: red;">Check this box to get a one day one-on-one coaching session with Michael
					at his office for $2,500 (regularly $6,000 for a day of private coaching)</span>-->

				</div>

				<br>
				<input type="image" src="images/index/button-apply-now.png"  />
			</form>

		</div>
		<!--END: Apply Now Form -->


	</div>
	<!-- END: Page Content -->

</div>

<?php include("footer.php"); ?>

<script src="js/qualification_validation.js"></script>
</body>
</html>