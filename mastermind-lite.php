<?php include("program/program.php"); ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8" />
<title>Six-Figure Martial Arts MasterMind</title>

<meta name="Description" content=""/>

<meta name="keywords" content=""/>


<link rel="stylesheet" href="https://www.ilovekickboxing.com/intl_css/reset.css"/>
<link rel="stylesheet" href="css/pages.css"/>
<link rel="stylesheet" href="css/mastermind_lite.css"/>


<script src="https://www.ilovekickboxing.com/intl_js/jquery.js"></script>
<!--[if lt IE 9]>
  <script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-29420623-21', 'martialarts100kmastermind.com');
  ga('send', 'pageview');

</script>


</head>
<body>
<?php include("header.php"); ?>

<div class="container">

	<!-- BEGIN: Page Content -->
	<div id="page_content">
		<?php include('navigation.php'); ?>

		<div id="headline-container">

			<h1>
        		Mike Parrella's 6-Figure MasterMind. For School Owners Ready<br>
				For Rockstar  Numbers... While Working Way Less Hours.
			</h1>

			<h2 class="futura">
				<strong>"My Six-Figure Mastermind is a<br />
				big investment. Not every school<br />
				owner  is ready to make it. That's<br />
				why I created Mastermind Lite."</strong>
			</h2>

			<p>
				It's a powerful coaching program designed to get you to the<br />
				point where the cost of the full Mastermind is chump change.
			</p>

		</div>

		<div class="copy">

			<article>
        		<p class="futura headline">Here's what you get, each and every month:</p>

				<p class="check">
            		<strong>1 LIVE 45-minute webinar every month,</strong> with a Q&amp;A
					session at the end.
				</p>
				<p class="check">
					<strong>2 Accountability & Action Coaching Calls per month</strong> where
					you get personal, detailed help to grow your school.
				</p>
				<p class="check">
					<strong>Access to the Private Facebook Group</strong> where everyone shares their
					tactics, secrets, and strategies for massive growth. (My staff and I
					contribute on these boards too, to give you even more help with
					your growth.)
				</p>
			</article>

			<article>
				<img class="right" src="images/m_lite/facebook.png" alt="" />
				<img class="right" src="images/m_lite/calculator.png" alt="" />
				<img class="right" src="images/m_lite/charts.png" alt="" />

             	<p class="futura headline">
					And here's just a taste of what you're going<br />
					to learn...
				</p>
				<p class="check">
                    <strong>How to use Private Facebook groups to increase your retention
					and student loyalty like crazy.</strong>
				</p>
				<p class="check">
					How to attract, manage, and create a rock star staff.
				</p>
				<p class="check">
					<strong>10+ idiot-proof ways to get more high-quality traffic to your website.</strong>
				</p>
				<p class="check">
					Is the decor and design of your school driving students away? Find out,
					and learn simple tweaks you can make that actually increase conversions.
				</p>
				<p class="check">
					<strong>The RIGHT way to calculate your numbers and stats to position
					yourself for unstoppable growth (Hint: I NEVER calculate my active
					student count. Instead, I calculate these other numbers...)</strong>
				</p>
				<p class="check">
					Offline marketing strategies that will bury your competition. Even if
					they try to copy you word-for-word.
				</p>
				<p class="check">
					<strong>How I enroll 100+ new members every single month from EACH of
					my locations. I'm handing you my blueprint for success.</strong>
				</p>
				<p class="check">
					Social Media Marketing: Why there's a 99% chance you're doing it all
					wrong, and what you should be doing instead.
				</p>
				<p class="btm-headline">
					<strong>Everything in Mastermind Lite is Cutting Edge.<br />
					These are my own Tested &amp; Proven methods.</strong>
				</p>
			</article>

			<a href="<?php echo $url_mastermind_lite; ?>">
        		<img class="apply_now" src="images/m_lite/button-register-now.png" alt="Apply Now" />
			</a>

			<article id="blueprint">
				<img class="right" src="images/m_lite/blueprint.png" alt="" />

				<p class="headline futura">
					I'm literally handing you my blueprint for<br />
					success, and giving you 2 coaching calls per<br />
					month to make sure you take action on it!
				</p>

				<p>
                	That means this isn't the same old sh*t you've been hearing for
					years. And it isn't the "same crap in a different box" you've been
					sold either.
				</p>
				<p>
					<strong>I care about your success.</strong> I tell it to you straight.
					And everything I share is out-of-the-box and original.
				</p>
				<p>
					In fact, you'll likely feel uncomfortable and even ashamed at times.
				</p>
				<p>
					<strong>But this is ESSENTIAL for success. Why?</strong>
				</p>
				<p>
					Because it means you're learning and growing. Before you can change
					what you're doing wrong, you have to accept you're doing things wrong.
				</p>
				<p>
					It doesn't feel good - but honestly, I go through it every day.
					That's why I am where I am today.
				</p>

			</article>

			<div class="clear"></div>
			<p class="futura sub-head">
				So if you too want to generate rockstar<br />
				numbers, while working way less hours,<br />
				welcome to Mastermind Lite.
			</p>

		</div>


		<a href="<?php echo $url_mastermind_lite; ?>">
        	<img class="apply_now" src="images/m_lite/button-register-now.png" alt="Apply Now" />
		</a>


	</div>
	<!-- END: Page Content -->

</div>

<?php include("footer.php"); ?>
</body>
</html>