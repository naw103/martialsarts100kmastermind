<?php include("program/program.php"); ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8" />
<title>Who is Michael Parrella? | Martial Arts MasterMind</title>

<meta name="Description" content=""/>

<meta name="keywords" content=""/>


<link rel="stylesheet" href="https://www.ilovekickboxing.com/intl_css/reset.css"/>
<link rel="stylesheet" href="css/pages.css"/>
<link rel="stylesheet" href="css/bio.css"/> 

<script src="https://www.ilovekickboxing.com/intl_js/jquery.js"></script>
<!--[if lt IE 9]>
  <script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-29420623-21', 'martialarts100kmastermind.com');
  ga('send', 'pageview');

</script>


</head>
<body>
<?php include("header.php"); ?>

<div class="container">

	<!-- BEGIN: Page Content -->
	<div id="page_content">
		<?php include('navigation.php'); ?>

		<div id="headline-container">

			<h1>
        		<!--Mike Parrella's 6-Figure MasterMind. For School Owners Ready<br>
				For Rockstar  Numbers... While Working Way Less Hours.-->
				A glimpse of the man you’ll be personally learning from.				
			</h1>

			<h2 class="futura"><strong>"Who <span class="futura-oblique">is</span> Mike Parrella?"</strong></h2>

		</div>

		<div class="copy">

			<p class="check">
				<strong>I own the fastest-growing fitness franchise in the U.S., with over 500 locations open or in development.</strong>
				Not only that - iLoveKickboxing generates more web specials for its locations than any studio in existence.
			</p>
			<p class="check">
				<strong>I have 8 locations of my own (with more on the way). Many of them generate 7-figures.</strong> 
				None of them generate less than multiple six-figures. In fact, some bring in $100k / month or more on average.
			</p>
			<p class="check">
				<strong>I manage all 8 locations in under 2 hours / week</strong> because of the systems & processes I’ve engineered.
			</p>
			<p class="check">
				<strong>My companies, FC Online Marketing & Parrella Consulting, help 1000+ schools achieve amazing success.</strong>
				As a direct result of our coaching & products, schools are reaching more people, serving them better, and achieving more personal &amp; financial freedom than ever.
			</p>
			<p class="check">
				<strong>In fact, Team Parrella is now the largest consulting company in the industry, for one simple reason:</strong> 
				We help schools get amazing results. Period. It's what we live for.
			</p>
			<p class="check">
				<strong>I started M.A. Business Summit which has grown from 200 - 700+ audience members since launching it 3 years ago.</strong>
				That’s because MABS is packed with refreshing, groundbreaking info that's truly helping schools grow.
			</p>
			<p class="check">
				<strong>I'm just passionate about our industry. I believe in what we do - and I love helping schools reach and help more people.</strong> Because at the end of the day - when I help you grow your school, I’m helping you impact more lives. And that makes me sleep like a baby at night, I gotta say ;-)
			</p>

		</div>

		
		<div class="content-divider">
			<img class="mike" src="images/bio/who-mike.jpg" alt="" width="363" />

			<p class="futura">
				"My biggest passion is helping school owners reach levels they only ever dreamed of."
			</p>
		</div>

		<a href="<?php echo $url_apply_now; ?>">
        	<img class="apply_now" src="images/index/button-apply-now.png" alt="Apply Now" />
		</a>


	</div>
	<!-- END: Page Content -->

</div>

<?php include("footer.php"); ?>
</body>
</html>