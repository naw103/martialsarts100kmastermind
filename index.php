<?php
	include("program/program.php");
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8" />
<title>Six-Figure Martial Arts MasterMind</title>

<meta name="Description" content=""/>

<meta name="keywords" content=""/>


<link rel="stylesheet" href="https://www.ilovekickboxing.com/intl_css/reset.css"/>
<link rel="stylesheet" href="css/pages.css"/>
<link rel="stylesheet" href="css/index.css"/>

<script src="https://code.jquery.com/jquery-1.9.1.js"></script>
<!--[if lt IE 9]>
  <script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-29420623-21', 'martialarts100kmastermind.com');
  ga('send', 'pageview');

</script>


</head>
<body>
<?php include("header.php"); ?>

<div class="container">

	<!-- BEGIN: Page Content -->
	<div id="page_content">
		<?php include('navigation.php'); ?>

		<div id="headline-container">

        	<h1>
                 Mike Parrella's 6-Figure MasterMind. For School Owners Ready<br>
				For Rockstar  Numbers... While Working Way Less Hours.
			</h1>

			<h4 class="futura">
            	"My locations consistently get 100+<br>
 				contracts a month. I spend 2-3 hours a<br>
				week managing all 8. In my MasterMind,<br>
				I reveal exactly how it's all done."
			</h4>

		</div>


		<div class="clear"></div>
		<div id="video-wrapper">

            <img class="arrow" src="images/index/watch-this-video.png" alt="Watch this video" />

			<video controls="controls" poster="https://www.martialarts100kmastermind.com/images/index/video.jpg" width="570" height="321">
				<source src="http://svn.ilovekickboxing.com/videos/converted-web/MartialArtsMastermindFilm.mp4" type="video/mp4" />
				<source src="http://svn.ilovekickboxing.com/videos/converted-web/MartialArtsMastermindFilm.ogv" type="video/ogg" />
				<object type="application/x-shockwave-flash" data="https://releases.flowplayer.org/swf/flowplayer-3.2.1.swf" width="570" height="322">
					<param name="movie" value="https://releases.flowplayer.org/swf/flowplayer-3.2.1.swf" />
					<param name="allowFullScreen" value="true" />
					<param name="wmode" value="transparent" />
					<param name="flashVars" value="config={'playlist':['http://www.martialarts100kmastermind.com/images/index/video.png',{'url':'http://svn.ilovekickboxing.com/videos/converted-web/MartialArtsMastermindFilm.mp4','autoPlay':false}]}" />
					<img alt="" src="https://www.martialarts100kmastermind.com/images/index/video.jpg" width="570" height="321" title="No video playback capabilities, please download the video below" />
				</object>
			</video>


	</div>


		<a href="<?php echo $url_apply_now; ?>">
        	<img class="apply_now" src="images/index/button-apply-now.png" alt="Apply Now" />
		</a>

        <!-- BEGIN: Note -->
		<div class="mastermind-copy">
			<p class="daniel">Friend,</p>
        	<p>
                Mike Parrella here, and here's the deal:
			</p>
			<p>
				My MasterMind has limited space. But there are a few openings left.
				Read on to see if it's right for you.
			</p>

			<p>
				<strong>Not everyone who applies gets in.</strong> And no, that's not just
				a &ldquo;pick up line&rdquo; ;-). People ready and willing to pay have been
				turned away because they weren't a good fit.
			</p>

			<p>
				This is an elite, top-level group of school owners who are action takers.
			</p>
			<p>
				If that sounds like you--and your adrenaline starts pumping when you
				think about doing what it takes to skyrocket your school's success--read on.
			</p>
			<p>
				Sounds like this could be a great fit.
			</p>
			<p>
				<strong>You should know though... This is NOT like any other mastermind
				out there.</strong>
			</p>
			<p>
				There are some "big names" in the industry who cram too many people into 
				a room... re-hash simple, stupid tactics... charge a ton of money... and 
				call it a "mastermind"...
			</p>
			<p>
				<strong>You won't find that here.</strong>
			</p>
			<p>
				I keep my MasterMind groups tight and focused. And the info is always FRESH, PROVEN, and POWERFUL.
			</p>
			<p>
				No one else is teaching what I'm teaching. That's because I invented
				these tactics myself.
			</p>
			<p>
				<strong>No one else in the industry consistently gets 100-150 enrollments
				per location each and every month... while working 2-3 hours a week. If
				you want to know how that's done, I'm the only guy who can teach you.</strong>
			</p>
			<p>
				And in the MasterMind, I will teach it to you. I hold nothing back. All
				of my knowledge, systems and tactics are yours for the taking.
			</p>
			<p>
				Peace,<br>
				<span class="daniel">MP</span>
			</p>
		</div>
        <!-- END: Note -->

		<!-- BEGIN: Who is Michael? -->
		<div id="who-is-michael">
			<img src="images/index/michae-parrella.png" alt="Michael Parrella's MasterMind" />
        	<p class="title futura">Who is Michael Parrella?</p>
			<p>
                <!--CEO of iLoveKickboxing.com, FC Online Marketing & highly successful school
				owner has continually rocked our industry with breakthrough online and offline
				marketing strategy. He's a featured columnist in MA Success, and his
				iLoveKickboxing.com franchise is responsible for almost 200,000 enrollments
				across schools worldwide in the past few years alone.-->
				CEO & Founder of iLoveKickboxing (the fastest-growing fitness franchise in the 
				nation), FC Online Marketing (insanely results-generating online marketing for 
				schools), & Team Parrella (the most successful and success-creating consulting 
				company in our industry)… Mike is here to help schools achieve success - while 
				having more fun, more freedom, and helping more people.
			</p>
			<p>
				“I love our industry. Martial arts is good for the world. I want to help as many 
				schools as I can grow, conquer, and flourish."
			</p>
		</div>
		<!-- END: Who is Michael -->

		<article>
        	<p class="headline futura">"90 Days to Success."</p>

			<img src="images/index/calendar.png" alt="" />

			<p>
                The goal of the first 90 days is simple:
			</p>
			<p>
				Figure out where you are now in your business, including
				every problem you're facing, frustrations you have, and
				what your numbers (financial's) are.
			</p>
			<p>
				Create a 90-day road map to get you to where you want
				to be.
			</p>
			<p>
				Sounds simple, right?
			</p>
			<p>
				That's because it is. MasterMind isn't about flowery language or complicated jargon.
			</p>
			<p>
				It's about results, plain and simple. And within the first 90 days,
				you'll already get incredible results.
			</p>
			<p>
				But that's just the first 3 months. After that there are still 9 more
				months of intensive growth, learning, and good times. And every session
				is jam-packed with more powerful business-growing info than you'd believe.
			</p>
		</article>


		<article>
        	<p class="headline futura">"I'll show you exactly how to..."</p>

			<p class="check futura">Work Less. Earn WAY More. Help Way More People</p>

			<p>
                <strong>Our industry is full of -- excuse my French -- bullshit.</strong>
				Bullshit work hours.<br>
				Bullshit tactics.
			</p>
			<p>
				If you follow what's been taught in our industry for decades, you can (if
				you're lucky) grow your school big.
			</p>
			<p>
				But you'll be a slave to your school. You'll work around the clock,
				6-7 days a week.
			</p>
			<p>
				If you get sick? Tough. Your school depends on you, so you better suck it
				up or you'll lose money. You probably already know this. You've likely
				experienced it.
			</p>
			<p>
				<strong>It doesn't have to be that way.</strong>
			</p>
			<p>
				I can show you how to make more money, help more students, and work
				WAY less. I manage 3 schools in 2-3 hours a week. Other MasterMind
				members do the same.
			</p>
			<p>
				You can do it too. You just have to follow my systems.
			</p>

		</article>


		<article id="second">
			<div class="horiz-row"></div>
        	<img class="right" src="images/index/chess-piece.png" alt="Martial Arts MasterMind" />
            <div class="horiz-row"></div>
			<img class="right" src="images/index/sunset.png" alt="Live Life Now" />
			<div class="horiz-row"></div>

            <br>
			<p class="check futura" style="margin-top: 0px;">
				Develop Cutting-Edge Online &amp; Offline<br>
				Marketing Strategies.
			</p>
			<p>
                I'm constantly testing and tweaking my marketing systems.
			</p>
			<p>
				<strong>Guess what happens every time I achieve a breakthrough?</strong>
			</p>
			<p>
				I share it with my MasterMind members.
			</p>
			<p>
				You'll get the latest tactics & strategies that I test on my own schools.
				I make sure they work. I fix all the &ldquo;bugs&rdquo; and &ldquo;kinks&rdquo;.
			</p>
			<p>
				Then I give them to you.
			</p>
			<p>
				<strong>No one else - including your competition - will be using the
				tactics that you're using. I guarantee it.</strong>
			</p>


            <br>
			<p class="check futura">Live life NOW. Don't Wait Till You're 65.</p>
			<p>
            	<strong>The biggest benefit MasterMind members get is time.</strong>
			</p>
			<p>
				Time for family. Time for you. Time for life.
			</p>
			<p>
				You can pursue your other passions and hobbies. You can be the best mom
				or dad in the world.
			</p>
			<p>
				You can travel the world.
			</p>
			<p>
				<strong>When you use my systems - and earn more, while working less - you
				can do all of these as much as you'd like.</strong>
			</p>


            <br><br>
			<p class="check futura">Overcome Any Challenge You Face in Your School.</p>
			<p>
            	Psychologists say that you're the sum of the 5 people you spend the
				most time with.
			</p>
			<p>
				<strong>In MasterMind, you only spend time with rockstar school owners.</strong>
			</p>
			<p>
				These guys and gals do what it takes to succeed. They achieve amazing
				numbers, while working insanely few hours.
			</p>
			<p>
				<strong>No matter what challenge you're facing... someone in the group
				has been through it and solved it.</strong>
			</p>
			<p>
				You'll be part of a network of incredible men and women who are there to
				help you. And you can offer your wisdom to help them, too.
			</p>
			<p>
				<strong>And if your problem is unique? I guarantee you if you bring it up
				to the group, you'll get the best solutions you could possibly imagine.</strong>
			</p>

        </article>



		<article>
            <div class="horiz-row"></div>
			<img class="right" src="images/index/china-houses.png" alt="Long Lasting Businesses" />
            <div class="horiz-row"></div>
			<img class="right" src="images/index/mastermind-members.png" alt="Work Hard & Have Fun" />
			<div class="horiz-row"></div>

            <br>
			<p class="check futura" style="margin-top: 0px;">Build a "1,000 Year Old" School.</p>
			<p>
                Japan is the only country in the world that has 1,000 year old businesses.
				They plan for the future. Most school owners? They're only concerned
				about right now.
			</p>
			<p>
				"How many students can I get today?"
			</p>
			<p>
				"How much money can I make this month?"
			</p>
			<p>
				<strong>Let me show you how to put systems in place so your school can
				last for generations. You can pass it on to your kids. They can pass it
				on to their kids.</strong>
			</p>
			<p>
				You can provide for generations to come.
			</p>


			<p class="check futura">Have Fun</p>
			<p>
            	MasterMind is fun. We all go out together. We hang out together. It's
				social. Stories are swapped. Friendships are born.
			</p>
			<p>
				The fun doesn't stop when the sessions do though.
			</p>
			<p>
				It continues when you're home. Running your school and your life should
				be fun.
			</p>
			<p>
				<strong>If it isn't fun now, don't worry. After 1 or 2 MasterMinds,
				it will be.</strong>
			</p>


            <br>
			<p class="check futura">Make Back Your Investment.</p>
			<p>
            	<strong>Every MasterMind member - yes, every one of them - has made
				back his or her investment.</strong>
			</p>
			<p>
				My game plan isn't to take your money and run.
			</p>
			<p>
				<strong>My game plan is to give you so much value, that I earn your
				trust. I only want to earn your trust through helping you achieve
				amazing results.</strong>
			</p>
			<p>
				I'll make you a deal:
			</p>
			<p>
				I'll give you the secret treasure map. You follow it.
			</p>
			<p>
				Deal?
			</p>

		</article>

		<a href="<?php echo $url_apply_now; ?>">
        	<img class="apply_now" src="images/index/button-apply-now.png" alt="Apply Now" />
		</a>


		<article>
        	<p class="headline futura">
				"More Perks of the<br>
				6-Figure MasterMind"
			</p>

			<img id="guarantee" src="images/index/guaranteed.png" alt="100% Money Back Guarantee" />

			<p class="check futura">100% Money Back Guarantee.</p>
			<p>
                There are no contracts with MasterMind. MasterMinds with contracts are
				scared. They're scared that you'll discover that they're worthless.
			</p>
			<p>
				They're scared that you'll want your money back.
			</p>
			<p>
				<strong>I'm not worried about that. Everyone in my MasterMind makes back
				their investment many times over.</strong>
			</p>
			<p>
				I believe in it so much that if you come to your first meeting... and
				decide it's not right for you... I'll refund your investment in full.
			</p>
			<p>
				Fair enough?
			</p>

		</article>


		<article>
			<p class="check futura">Royal Treatment</p>

			<img class="full" src="images/index/royal-treatment.jpg" alt="Royal Treatment" />

			<p>
                I once joined a MasterMind that cost me an arm and a leg. When I showed
				up, they didn't even provide us with water.
			</p>
			<p>
				I didn't feel very special.
			</p>
			<p>
				<strong>In my MasterMind, you get the Royal Treatment:</strong>
			</p>
			<p>
				Lunch and Breakfast included every day.
			</p>
			<p>
				I take you and everyone out for a very nice dinner on the first night of every session.
			</p>
			<p>
				Refreshments &amp; snacks provided.
			</p>
			<p>
				And more.
			</p>
			<p>
				<strong>These perks don't cost extra. MasterMind members are an elite group.
				You deserve to be treated like it.</strong>
			</p>

		</article>


		<article>
			<p class="check futura">Learn from the ONLY person in our industry who...</p>

			<img class="full" src="images/index/attention.png" alt="" />

			<p>
				<strong>Has facilities that consistently generate 100-150 members a month.</strong>
				 This doesn't happen every once in a while for me.
			</p>
			<p>
				It happens every month of the year. Let me show you how.
			</p>
			<p>
				<strong>Makes schools profitable within 4-5 months of opening.</strong>
			</p>
			<p>
				I haven't come across anyone else in our industry who's done this. Have you?
			</p>
			<p>
				<strong>Gets schools to generate $500k - $600k in their first year of opening.</strong>
			</p>
			<p>
				Same as the one above. If you've met someone else who's done this, send me
				their info. I haven't come across anyone else who can consistently pull
				it off.
			</p>
			<p>
				<strong>Achieves all of this in 2-3 hours a week.</strong> Sure, when you
				first open a school you'll have to be more hands on. When you first start
				implementing my systems, you'll work more than 2-3 hours.
			</p>
			<p>
				But once you put my systems in place, you can run and grow your school in
				a single afternoon per week.
			</p>

		</article>


		<article>
			<p class="check futura">Travel to the coolest spots in the country.</p>
			<p>
            	<strong>Sessions take place in a new city every time. New York...
				Miami... San Diego... And more.</strong>
			</p>
			<p>
				Break away from the scenery of your day-to-day life every time there's a session.
			</p>

			<img class="full" src="images/index/beautiful-desitinations.png" alt="" />

		</article>


		<a href="<?php echo $url_apply_now; ?>">
        	<img class="apply_now" src="images/index/button-apply-now.png" alt="Apply Now" />
		</a>


		<p class="headline futura">"Some Important Logistics &amp; FAQ"</p>

		<!-- BEGIN: Accordion -->
		<div id="accordion">
			<h3 class="futura_lite">"How many times a year do we meet?"</h3>
			<div>
				<p>
					MasterMind meets 4 times a year. In every meeting, you leave with a
					solid, actionable plan. There's no &ldquo;theory&rdquo; or &ldquo;fluff&rdquo;.
					You only get methods I've proven myself.
				</p>
				<p>
					Between meetings, you have complete access to me for any questions or
					problems you face. You're never stranded or alone. I've always got
					you covered.
				</p>
			</div>
            <!--
			<h3 class="futura_lite">"How long is each meeting?"</h3>
			<div>
				<p>
					MasterMind meets 4 times a year. In every meeting, you leave with a
					solid, actionable plan. There's no &ldquo;theory&rdquo; or &ldquo;fluff&rdquo;.
					You only get methods I've proven myself.
				</p>
				<p>
					Between meetings, you have complete access to me for any questions or
					problems you face. You're never stranded or alone. I've always got
					you covered.
				</p>
			</div>

			<h3 class="futura_lite">"What happens if I miss one?"</h3>
			<div>
				<p>
					MasterMind meets 4 times a year. In every meeting, you leave with a
					solid, actionable plan. There's no &ldquo;theory&rdquo; or &ldquo;fluff&rdquo;.
					You only get methods I've proven myself.
				</p>
				<p>
					Between meetings, you have complete access to me for any questions or
					problems you face. You're never stranded or alone. I've always got
					you covered.
				</p>
			</div>
            -->

		</div>
		<!-- END: Acordion -->


		<article>
			<p class="headline futura">
				"Who MasterMind<br>
				Is and Isn't For:"
			</p>

        	<p class="wrong futura">MasterMind isn't for...</p>
			<p>
            	<strong>Anyone looking to become an instant millionaire overnight.</strong>
				You're not willing to do what it takes to lay down the groundwork for a
				long-lasting, stable, lucrative business. This isn't for you.
			</p>
			<p>
				<strong>Anyone who thinks they "deserve" success, or it
				should just "come to them".</strong> MasterMind members realize that no
				one owes you anything. They realize
				that you reap what you sow. They're not afraid of hard work. They relish in it.
			</p>
			<p>
				<strong>Anyone who doesn't see the value in paying for mentorship.</strong>
				If that's you, I'm not sure why you've even read this far. I pay lots
				of money for mentors. I believe
				everyone should have a mentor. If you don't see the value in that,
				this isn't for you.
			</p>
		</article>


		<article>
			<p class="check futura">Mastermind is for...</p>
			<p>
            	<strong>Anyone looking to grow a stable, long-lasting, lucrative business.</strong>
				You don't want a quick paycheck or "tricks" to success. You want to build your
				business the right way. You're okay with doing the work today, getting paid (a lot) tomorrow.
			</p>
			<p>
				<strong>Anyone who doesn't wait around for "life" to bring
				them success.</strong> MasterMind members go out and get success. They
				don't sit around and wonder why it never comes to them. If this is you,
				you'll fit right in.
			</p>
			<p>
				<strong>You see the value in paying for mentorship, and you jump on
				opportunity when it presents itself.</strong> I practice what I preach.
				I pay a lot for mentors. I jump on opportunity like my life depended on
				it. If you value these things too, you'll fit right in.
			</p>

		</article>

		<p class="headline futura">
			Space is Highly Limited.<br>
			Apply Now.
		</p>

		<a href="<?php echo $url_apply_now; ?>">
        	<img class="apply_now" src="images/index/button-apply-now.png" alt="Apply Now" />
		</a>


		<div class="mastermind-copy">
        	<p>
                Word of the results my MasterMind members get is spreading like wildfire.
			</p>
			<p>
				It's growing all the time.
			</p>
			<p>
				Don't hesitate on this. Apply now, and get the ball rolling.
			</p>
			<p>
				If you're a serious action taker who's willing to empty your "cup"...
				and take action on the systems and secrets I'm going to give you...
			</p>
			<p>
				I - and everyone else in the group - wants you to be here.
			</p>
			<p>
				Apply now by clicking the link below.
			</p>
			<p>
				<strong>The way I see it, you have two options:</strong>
			</p>
			<p>
				1. Keep doing what you're doing, and generating the same results. The same
				hours. The same struggles.
			</p>
			<p>
				2. Learn from me and the other MasterMind members and break free of your
				"golden handcuffs". Enjoy your life. Enjoy your family. Enjoy your time.
			</p>
			<p>
				<strong>It ain't cheap, but everyone in MasterMind agrees that it's still
				under priced. And everyone has made back their investment many times over.</strong>
			</p>
			<p>
				<strong>And with my 100% Money Back Guarantee, there's really nothing to lose.</strong>
			</p>
			<p>
				This is a no-brainier.
			</p>
			<p>
				Make the right call,
			</p>
			<p class="daniel">
				Mike Parrella
			</p>
		</div>
        <br>
		<a href="<?php echo $url_apply_now; ?>">
        	<img class="apply_now" src="images/index/button-apply-now.png" alt="Apply Now" />
		</a>


	</div>
	<!-- END: Page Content -->

</div>

<?php include("footer.php"); ?>

</body>
</html>