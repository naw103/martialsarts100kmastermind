<?php
session_start();

if(!isset($_SESSION['userid'])):
    header("Location: index.php");
endif;

include("program/program.php");
?>
<!DOCTYPE html>
<html>
<head>
<title>Member Area</title>

<link rel="stylesheet" href="http://www.ilovekickboxing.com/intl_css/reset.css" />
<link rel="stylesheet" href="css/admin_pages.css" />
<link rel="stylesheet" href="css/styles.css" />

</head>
<body>

<?php include("header.php"); ?>

<div class="container">

	<!-- BEGIN: Page Content -->
	<div id="page_content">
        <?php include('navigation.php'); ?>

        <div id="wrapper">

            <h2>MasterMind Applicants</h2>


            <table class="applicants">
                <tr>
                     <th>Name</th>
                     <th>Email</th>
                     <th>Submission Date</th>
                </tr>

                <?php for($i = 0; $i < count($applicants); $i++): ?>
                <tr data-id="<?php echo $applicants[$i]['id']; ?>">
                     <td><?php echo $applicants[$i]['fullname']; ?></td>
                     <td><?php echo $applicants[$i]['email']; ?></td>
                     <td><?php echo date('M d y',strtotime($applicants[$i]['submit_date'])); ?></td>
                </tr>
                <?php endfor; ?>
            </table>


        </div>

	</div>
	<!-- END: Page Content -->

</div>

<?php include("footer.php"); ?>

<div id="overlay"></div>
<div id="applicant-container">
     <h3 id="name"></h3>

     <div class="content">Email: <span id="email"></span></div> 

	 <div class="content">Address: <span id="address"></span></div>
	 <div class="content">City: <span id="city"></span></div>
	 <div class="content">Zip Code: <span id="zipcode"></span></div>
	 <div class="content">State: <span id="state"></span></div>

     <div class="content">
        Descriptions of your business (How long in business, how many members, how much
        monthly gross revenue do you bring in annually)
        <div id="business_description"></div>
     </div>

     <div class="content">
        What obstacles and challenges have you encountered in life and business and how did you overcome them?
        <div id="business_obstacles"></div>
     </div>

     <div class="content">
        What do you feel are the top challenges you're having in your business?
        <div id="business_challenges"></div>
     </div>

     <div class="content">
        Briefly describe why you feel you're a good candidate for the "6 Figure Mastermind" Coaching Program:
        <div id="business_candidate"></div>
     </div>

     <div class="content">
        What are your business goals for the next 12 months?
        <div id="business_goals"></div>
     </div>

     <div class="content">
        What's important for Michael to know about you personally?
        <div id="important_note"></div>
     </div>


</div>

<script src="js/view_profile.js"></script>
</body>
</html>