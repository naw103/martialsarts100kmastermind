<?php
  session_start();
  if(isset($_SESSION['userid'])):
    header("Location: member_area.php");
  endif;

?>
<!DOCTYPE html>
<html>
<head>
<title>Admin Login</title>

<link rel="stylesheet" href="http://www.ilovekickboxing.com/intl_css/reset.css" />
<link rel="stylesheet" href="css/admin_pages.css" />
<link rel="stylesheet" href="css/login.css" />

</head>
<body>

<?php include("header.php"); ?>

<div class="container">

	<!-- BEGIN: Page Content -->
	<div id="page_content">
        <?php include('navigation.php'); ?>

        <div id="form-wrapper">

        <p class="head">Admin Login</p>

        <?php if(isset($_SESSION['msg'])): ?>
        <p class="error"><?php echo $_SESSION['msg']; ?></p>
        <?php endif; ?>

        <form action="program/login.php" method="post">

            <label for="username">Username:</label>
            <input type="text" name="username" /><br />

            <label for="password">Password:</label>
            <input type="password" name="password" /><br />

            <input type="submit" value="Login" />
        </form>

    </div>

	</div>
	<!-- END: Page Content -->

</div>

<?php include("footer.php"); ?>

</body>
</html>

