<!-- BEGIN: Footer -->
<footer>
	<p>All Rights Reserved &copy; Six-Figure Martial Arts Mastermind from Mike Parrella 2013.</p>
</footer>
<!-- END: Footer -->

<!--<script src="http://www.ilovekickboxing.com/intl_js/jquery.js"></script>-->
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script src="http://www.ilovekickboxing.com/intl_js/cufon.js"></script>
<script src="js/fonts/Futura-CondensedMedium_500.font.js"></script>
<script src="js/fonts/Daniel_400.font.js"></script>
<script src="js/fonts/Futura_Std_300.font.js"></script>
<script>
	Cufon.replace('.futura', {fontFamily: 'Futura-CondensedMedium' });
	Cufon.replace('.futura_lite', {fontFamily: 'Futura Std Lite' });
	Cufon.replace('.daniel', {fontFamily: 'Daniel' });
</script>

<?php
  $ie = strpos($_SERVER["HTTP_USER_AGENT"],"MSIE");
  if(!$ie):
?>
<script>Cufon.now();</script>
<?php endif; ?>

 <script>

$(function() {
  $( "#accordion" ).accordion();
});

$(document).ready(function(){

    $('#accordion h3').click(function(){
       Cufon.refresh();
    });

    Cufon.refresh();

});

</script>