$(document).ready(function(){

    $('td').click(function(){

        var uid = $(this).parent('tr').attr('data-id');

        $.ajax({
            url: 'program/get_applicant.php',
            type: "POST",
            dataType: "xml",
            data: { userid: uid },
            success: function(xml)
            {
                $(xml).find('applicant').each(function() {
                    fullname = $(this).find('fullname').text();
					//submit_date = $(this).find('submit_date').text();
                    email = '<a href="mailto:' + $(this).find('email').text() + '">' + $(this).find('email').text() + '</a>';
                    address =  $(this).find('address').text();
                    city = $(this).find('city').text();
					zipcode = $(this).find('zipcode').text();
					state = $(this).find('statecode').text();

                    business_description = $(this).find('business_description').text();
                    business_obstacles = $(this).find('business_obstacles').text();
                    business_challenges = $(this).find('business_challenges').text();
                    business_candidatess = $(this).find('business_candidatess').text();
                    business_goals = $(this).find('business_goals').text();

                    important_note = $(this).find('important_note').text();
                });

				$('#overlay').show();
				$('#overlay').width($(document).width());
				$('#overlay').height($(document).height());

                $('#applicant-container').css("top", Math.max(0, (($(window).height() - $('#applicant-container').outerHeight()) / 2) +
                                                $(window).scrollTop()) + "px");
    			$('#applicant-container').css("left", Math.max(0, (($(window).width() - $('#applicant-container').outerWidth()) / 2) +
                                                $(window).scrollLeft()) + "px");


                $('#applicant-container #name').html(fullname);

				$('#applicant-container #address').html(address);

                $('#applicant-container #email').html(email);
				
                $('#applicant-container #city').html(city);
                $('#applicant-container #zipcode').html(zipcode);
				$('#applicant-container #state').html(state);

                $('#applicant-container #business_description').html(business_description);
                $('#applicant-container #business_obstacles').html(business_obstacles);
                $('#applicant-container #business_challenges').html(business_challenges);
                $('#applicant-container #business_candidate').html(business_candidatess);
                $('#applicant-container #business_goals').html(business_goals);

                $('#applicant-container #important_note').html(important_note);
                //$('#applicant-container #submit_date').html(submit_date);
                $('#applicant-container').show();
            }

        }); // end ajax

    }); // end click

	$('#overlay').click(function(){
		$('#applicant-container').hide();
		$('#overlay').hide();
	});

}); // end jquery