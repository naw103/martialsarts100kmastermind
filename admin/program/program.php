<?php
$url_apply_now = "apply-now.php";

include('definitions.php');
include('database.class.php');

$config = new config(DB_HOST, DB_USER, DB_PASS, DB_NAME);

// create db class
$db = new database($config);

// open connection to database
$db->openConnection();

$sql = "SELECT * FROM mp_mastermind";

$result = $db->query($sql);

 while($row = $db->fetchAssoc($result)):
    $applicants[] = $row;
 endwhile;

// kill database connection
$db->closeConnection();

?>