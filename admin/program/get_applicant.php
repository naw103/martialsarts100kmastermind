<?php

$userid = $_POST['userid'];

include('definitions.php');
include('database.class.php');

$config = new config(DB_HOST, DB_USER, DB_PASS, DB_NAME);

// create db class
$db1 = new database($config);

// open connection to database
$db1->openConnection();

$sql = "SELECT * FROM mp_mastermind WHERE id = $userid";

$result = $db1->query($sql);

$applicant = $db1->fetchAssoc($result);

// Get state info based on statecode
$statecode = $applicant['statecode'];

$sql = "SELECT abbreviation FROM states WHERE id = $statecode";

$result = $db1->query($sql);

$state = $db1->fetchAssoc($result);

// Start our XML
header("Content-type: text/xml");
header("Cache-Control: no-cache");

$response = '<?xml version="1.0" encoding="utf-8" ?>';
$response .= '<application>';
    $response .= '<applicant>';
        $response .= '<fullname>' . $applicant['fullname'] . '</fullname>';

		$response .= '<email>' . $applicant['email'] . '</email>';
        $response .= '<address>' . $applicant['address'] . '</address>';

		/*$response .= '<submit_date>' . date('M d y',strtotime($applicant['submit_date'])) . '</submit_date>'; */

        $response .= '<city>' . $applicant['city'] . '</city>';
        $response .= '<zipcode>' . $applicant['zipcode'] . '</zipcode>';
		$response .= '<statecode>' . $state['abbreviation'] . '</statecode>';

        $response .= '<business_description>' . $applicant['business_description'] . '</business_description>';
        $response .= '<business_obstacles>' . $applicant['business_obstacles'] . '</business_obstacles>';
        $response .= '<business_challenges>' . $applicant['business_challenges'] . '</business_challenges>';
        $response .= '<business_candidate>' . $applicant['business_candidate'] . '</business_candidate>';
        $response .= '<business_goals>' . $applicant['business_goals'] . '</business_goals>';

        $response .= '<important_note>' . $applicant['important_note'] . '</important_note>';

    $response .= '</applicant>';
$response .= '</application>';

echo $response;
// kill database connection
$db1->closeConnection();

?>