<?php
session_start();

// get passed data
$username = $_POST['username'];
$pwd = $_POST['password'];

include('definitions.php');
include('database.class.php');

$config = new config(DB_HOST, DB_USER, DB_PASS, DB_NAME);

// create db class
$db = new database($config);

// open connection to database
$db->openConnection();

$sql = "SELECT id FROM admin_users WHERE username = '$username' AND password = '$pwd'";

$result = $db->query($sql);

$user = $db->fetchAssoc($result);

if($db->countRows($result)):
    // success
    // user exists -> go to members area

    $_SESSION['userid'] = $user['id'];
    header("Location: ../member_area.php");

else:
    // failed
    // user does not exist  -> back to login page
    $_SESSION['msg'] = "Wrong username and/or password";
    header("Location: ../index.php");
endif;

// kill database connection
$db->closeConnection();

?>