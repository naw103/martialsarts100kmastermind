<?php

class database
{
    private $connection;
    private $selectdb;
    private $queryResult;
    private $config;

    function __construct($config)
    {
        $this->config = $config;
    }

    function __destruct()
    {

    }

    public function openConnection()
    {
        try
        {
            $this->connection = mysql_connect($this->config->hostname, $this->config->username, $this->config->password);
            $this->selectdb = mysql_select_db($this->config->database);
        }
        catch(exception $e)
        {
            return $e;
        }

    }

    public function closeConnection()
    {
        try
        {
            mysql_close($this->connection);
        }
        catch(exception $e)
        {
            return $e;
        }

    }


    public function escapeString($string)
    {
        return mysql_real_escape_string($string);
    }

    public function query($query)
    {
        $query = str_replace("}", "", $query);
        $query = str_replace("{", $this->config->prefix, $query);

        try
        {
            if(empty($this->connection))
            {
                $this->openConnection();

                $this->queryResult = mysql_query($query);

                $this->closeConnection();

                return $this->queryResult;
            }
            else
            {
                $this->queryResult = mysql_query($query);

                return $this->queryResult;
            }

        }
        catch(excpetion $e)
        {
            return $e;
        }

    }

    public function queryResult()
    {
        return $this->queryResult;
    }

    public function hasRows($result)
    {
        try
        {
            if(mysql_now_rows($result) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        catch(exception $e)
        {
            return $e;
        }
    }

    public function countRows($result)
    {
        try
        {
            return mysql_num_rows($result);
        }
        catch(exception $e)
        {
            return $e;
        }

    }


    public function fetchAssoc($result)
    {
        try
        {
            return mysql_fetch_assoc($result);
        }
        catch(exception $e)
        {
            return $e;
        }
    }

    public function fetchArray($result)
    {
        try
        {
            return mysql_fetch_array($result);
        }
        catch(exception $e)
        {
            return $e;
        }
    }

}



class config
{
    public $hostname;
    public $username;
    public $password;
    public $database;
    public $prefix;
    public $connector;

    function __construct($hostname = NULL, $username = NULL, $password = NULL, $database = NULL, $prefix = NULL, $connector = NULL)
    {
        $this->hostname = !empty($hostname) ? $hostname : "";
        $this->username = !empty($username) ? $username : "";
        $this->password = !empty($password) ? $password : "";
        $this->database = !empty($database) ? $database : "";
        $this->prefix = !empty($prefix) ? $prefix : "";
        $this->connector = !empty($connector) ? $connector : "";
    }

    function __destruct()
    {

    }


}



?>