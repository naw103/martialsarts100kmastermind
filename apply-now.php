<?php
	include("program/program.php");
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8" />
<title>Six-Figure Martial Arts MasterMind</title>

<meta name="Description" content=""/>

<meta name="keywords" content=""/>


<link rel="stylesheet" href="https://www.ilovekickboxing.com/intl_css/reset.css"/>
<link rel="stylesheet" href="css/pages.css"/>
<link rel="stylesheet" href="css/apply.css"/>


<script src="https://www.ilovekickboxing.com/intl_js/jquery.js"></script>
<!--[if lt IE 9]>
  <script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-29420623-21', 'martialarts100kmastermind.com');
  ga('send', 'pageview');

</script>


</head>
<body>
<?php include("header.php"); ?>

<div class="container">

	<!-- BEGIN: Page Content -->
	<div id="page_content">
		<?php include('navigation.php'); ?>

		<div id="headline-container">

			<h1>
        		Mike Parrella's 6-Figure MasterMind. For School Owners Ready<br>
				For Rockstar  Numbers... While Working Way Less Hours.
			</h1>

			<h2 class="futura">
				Apply now, and get ready for<br>
				the most amazing experience<br>
				of your life.
			</h2>

		</div>

		<!-- BEGIN: Apply Now Form -->
		<div id="form-wrapper">
        	<form method="post" class="af-form-wrapper" action="https://www.aweber.com/scripts/addlead.pl"  >
				<div style="display: none;">
					<input type="hidden" name="meta_web_form_id" value="296779819" />
					<input type="hidden" name="meta_split_id" value="" />
					<input type="hidden" name="listname" value="parrellamasterm" />
					<!--<input type="hidden" name="redirect" value="https://www.aweber.com/thankyou.htm?m=default" id="redirect_4528816d984bb36d74e4c7cd5ae30de8" />-->
                   <!-- <input type="hidden" name="redirect" value="https://martialarts100kmastermind.com/application.php" id="redirect_4528816d984bb36d74e4c7cd5ae30de8" />
					<input type="hidden" name="meta_redirect_onlist" value="https://martialarts100kmastermind.com/application.php" />
					-->
					<input type="hidden" name="redirect" value="https://martialarts100kmastermind.com/signup.php" id="redirect_4528816d984bb36d74e4c7cd5ae30de8" />
					<input type="hidden" name="meta_redirect_onlist" value="https://martialarts100kmastermind.com/signup.php" />
					<input type="hidden" name="meta_adtracking" value="My_Web_Form" />
					<input type="hidden" name="meta_message" value="1" />
					<input type="hidden" name="meta_required" value="name,email" />
					<input type="hidden" name="meta_tooltip" value="" />
				</div>

				<label class="previewLabel" for="awf_field-53172502">Name: </label>
				<input id="awf_field-53172502" type="text" name="name" class="med  text" value=""  tabindex="500" />

				<label class="previewLabel" for="awf_field-53172503">Email: </label>
				<input class="med text" id="awf_field-53172503" type="text" name="email" value="" tabindex="501"  />


				<label class="previewLabel" for="awf_field-53172504" style="display: none;">Website URL:</label>
				<input type="hidden" id="awf_field-53172504" class="med  text" name="custom Website URL" value=''  tabindex="502" />

				<label class="previewLabel" for="awf_field-53172505">Comments/<br />Questions:</label>
				<textarea  name="custom Comments" id="awf_field-53172505"  tabindex="503" ></textarea>

				<input name="submit" class="submit" type="image" src="images/index/button-apply-now.png" tabindex="504" />

			</form>
		</div>
		<!--END: Apply Now Form -->


	</div>
	<!-- END: Page Content -->

</div>

<?php include("footer.php"); ?>

<script>
  // clear all input
  $('input[type="text"], textarea').val('');

  $(document).ready(function(){

  	$('form').submit(function(){

		// check out text inputs
		$('input[type="text"]').each(function(){

			if($(this).val() == '' && ($(this).attr('name') != 'custom Website URL'))
			{
				$(this).addClass('error');
			}
			else
			{
				$(this).removeClass('error');
			}

		});

		if($('.error').length > 0)
		{
        	return false;
		}

	});


  });
</script>
</body>
</html>