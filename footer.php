<!-- BEGIN: Footer -->
<footer>
	<p style="color: #FFF;">
						<a href="index.php" style="color: #FFF;">Home</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
						<a href="privacy.php" style="color: #FFF;">Privacy Policy</a>
					</p>
	<p>All Rights Reserved &copy; Six-Figure Martial Arts Mastermind from Mike Parrella <?php echo date('Y'); ?>.</p>
</footer>
<!-- END: Footer -->

<!--<script src="https://www.ilovekickboxing.com/intl_js/jquery.js"></script>-->
<script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script src="https://www.ilovekickboxing.com/intl_js/cufon.js"></script>
<script src="https://www.martialarts100kmastermind.com/js/fonts/Futura-CondensedMedium_500.font.js"></script>
<script src="https://www.martialarts100kmastermind.com/js/fonts/Daniel_400.font.js"></script>
<script src="https://www.martialarts100kmastermind.com/js/fonts/Futura_Std_300.font.js"></script>
<script src="https://www.martialarts100kmastermind.com/js/fonts/Futura_Std_oblique_500.font.js"></script>
<script src="https://www.martialarts100kmastermind.com/js/fonts/Futura_Std_Bold_Condensed.font.js"></script>
<script>
	Cufon.replace('.futura', {fontFamily: 'Futura-CondensedMedium' });
	Cufon.replace('.futura_lite', {fontFamily: 'Futura Std Lite' });
	Cufon.replace('.futura-oblique', {fontFamily: 'Futura Std' });
	Cufon.replace('.futura-bold', {fontFamily: 'Futura Std Bold' });
	Cufon.replace('.daniel', {fontFamily: 'Daniel' });
</script>

<?php
  $ie = strpos($_SERVER["HTTP_USER_AGENT"],"MSIE");
  if(!$ie):
?>
<script>Cufon.now();</script>
<?php endif; ?>

<?php
    /* INDEX ONLY PAGE */
    if(strpos($_SERVER['PHP_SELF'], 'index.php')):
?>

<script>

$(function() {
  $( "#accordion" ).accordion();
});

$(document).ready(function(){

    $('#accordion h3').click(function(){
       Cufon.refresh();
    });

    Cufon.refresh();

});

</script>
<?php endif; ?>