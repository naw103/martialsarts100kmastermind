$(document).ready(function(){



$('form').submit(function(){

    // check text & textarea
    $('textarea, input[type="text"]').each(function(){

        if($(this).val() == '' && ($(this).attr('name') != 'address1'))
        {
            $(this).addClass('error');
        }
        else
        {
            $(this).removeClass('error');
        }

    });

    // check pay option radio button
    if(!$('input[name="year_2012"]').is(":checked"))
    {
        $('input[name="year_2012"]').parent('.inner_wrapper').addClass('error');
    }
    else
    {
        $('input[name="year_2012"]').parent('.inner_wrapper').removeClass('error');
    }

    // check pay option radio button
    if(!$('input[name="year_2011"]').is(":checked"))
    {
        $('input[name="year_2011"]').parent('.inner_wrapper').addClass('error');
    }
    else
    {
        $('input[name="year_2011"]').parent('.inner_wrapper').removeClass('error');
    }

    // check pay option radio button
    if(!$('input[name="year_2010"]').is(":checked"))
    {
        $('input[name="year_2010"]').parent('.inner_wrapper').addClass('error');
    }
    else
    {
        $('input[name="year_2010"]').parent('.inner_wrapper').removeClass('error');
    }

    // check pay option radio button
    if(!$('input[name="pay_option"]').is(":checked"))
    {
        $('input[name="pay_option"]').parent('.wrapper').addClass('error');
    }
    else
    {
        $('input[name="pay_option"]').parent('.wrapper').removeClass('error');
    }

    // check state select
    if($('select option:selected').val() == 0)
    {
        $('select').addClass('error');
    }
    else
    {
        $('select').removeClass('error');
    }

     if($('.error').length > 0)
        {
			alert("You must enter all information");
            return false;
        }

});

$('table td input[type="text"]').keydown(function(event) {

        // Allow: backspace, delete, tab, escape, and enter
        if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
             // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) ||
             // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        else {
            // Ensure that it is a number and stop the keypress
            if(event.keyCode == 190){
              return;
            }

            if(event.keyCode == 110){
              return;
            }

            if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                event.preventDefault();
                if($(this).siblings('div.keyError').length == 0){
                $('<span class="keyError">Only numbers are allowed</span>').insertAfter($(this));
                }
            }
            else
            {
              $(this).siblings('div.keyError').remove();
            }
        }
    }); // end keydown

});