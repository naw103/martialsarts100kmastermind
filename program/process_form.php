<?php
require('database.php');

dbConn();

date_default_timezone_set('America/New_York');
$date = date("Y-m-d H:i:s");
$q = "INSERT INTO mp_mastermind (fullname, address, address1, city, statecode, zipcode, phone, email, business_growth_2014,
								 business_growth_2013, business_growth_2012, payment_option, coaching_option,
								 business_description, business_obstacles, business_challenges, business_candidate,
								 business_goals, important_note, rating_clarity, rating_time_mangement, rating_outsourcing,
								 rating_prices, rating_customer_service, rating_competition, rating_copywriting, rating_email_strategy,
								 rating_creation, rating_program_launch, rating_traffic_strategies, rating_tracking_testing,
								 rating_conversion, rating_affiliate, rating_offline_marketing, rating_marketing, rating_time_allocated,
								 rating_systematic, rating_personal_productivity, rating_passion, rating_income_growth, rating_relationships,
								 submit_date )
					         VALUES ('%s','%s','%s','%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s',
							 		 '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s',  '%s', '%s', '%s',
									 '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s',
									 '%s', '%s', '%s', '%s', '%s', '$date')";


$query = sprintf($q,
        		mysql_real_escape_string($_POST['fullname']),
                mysql_real_escape_string($_POST['address']),
                mysql_real_escape_string($_POST['address1']),
				mysql_real_escape_string($_POST['city']),
				mysql_real_escape_string($_POST['statecode']),
				mysql_real_escape_string($_POST['zipcode']),
				mysql_real_escape_string($_POST['phone']),
				mysql_real_escape_string($_POST['email']),
				mysql_real_escape_string($_POST['year_2012']),
				mysql_real_escape_string($_POST['year_2011']),
				mysql_real_escape_string($_POST['year_2010']),
				mysql_real_escape_string($_POST['pay_option']),
				mysql_real_escape_string($_POST['coaching_option']),
				mysql_real_escape_string($_POST['business_description']),
				mysql_real_escape_string($_POST['business_obstacles']),
				mysql_real_escape_string($_POST['business_challenges']),
				mysql_real_escape_string($_POST['business_candidate']),
				mysql_real_escape_string($_POST['business_goals']),
				mysql_real_escape_string($_POST['important_note']),
				mysql_real_escape_string($_POST['rating_clarity']),
				mysql_real_escape_string($_POST['rating_time_mangement']),
				mysql_real_escape_string($_POST['rating_outsourcing']),
                mysql_real_escape_string($_POST['rating_prices']),
				mysql_real_escape_string($_POST['rating_customer_service']),
				mysql_real_escape_string($_POST['rating_competition']),
				mysql_real_escape_string($_POST['rating_copywriting']),
				mysql_real_escape_string($_POST['rating_email_strategy']),
				mysql_real_escape_string($_POST['rating_creation']),
				mysql_real_escape_string($_POST['rating_program_launch']),
				mysql_real_escape_string($_POST['rating_traffic_strategies']),
				mysql_real_escape_string($_POST['rating_tracking_testing']),
				mysql_real_escape_string($_POST['rating_conversion']),
				mysql_real_escape_string($_POST['rating_affiliate']),
				mysql_real_escape_string($_POST['rating_offline_marketing']),
				mysql_real_escape_string($_POST['rating_marketing']),
				mysql_real_escape_string($_POST['rating_time_allocated']),
				mysql_real_escape_string($_POST['rating_systematic']),
				mysql_real_escape_string($_POST['rating_personal_productivity']),
				mysql_real_escape_string($_POST['rating_passion']),
				mysql_real_escape_string($_POST['rating_income_growth']),
				mysql_real_escape_string($_POST['rating_relationships'])
		);

$result = mysql_query($query);

if($result):
    $statecode = $_POST['statecode'];
	$sql = "SELECT abbreviation FROM states WHERE id = $statecode";

	$data = mysql_query($sql);

	while($row = mysql_fetch_assoc($data)):
		$state = $row;
	endwhile;

	if($_POST['coaching_option'] == ''):
		$coaching_option = 'No';
	else:
        $coaching_option = 'Yes';
	endif;

	//$to      = 'ryan@fconlinemarketing.com';
	$to      = 'ryan@fconlinemarketing.com, michael@fconlinemarketing.com, kim@parrellaconsulting.com, scott@fconlinemarketing.com';
    //$to = 'chris@fconlinemarketing.com';
	$subject = 'MasterMind Application';

	$message = "Contact Information" . "\n" . "\n";

	$message .= 'Name: ' . ucwords($_POST['fullname']) . "\n";
	$message .= 'Phone: ' . $_POST['phone'] . "\n";
	$message .= 'Address: ' . $_POST['address'] . ' ' . $_POST['address1'] . "\n";
	$message .= 'City: ' . $_POST['city'] . "\n";
	$message .= 'State: ' . $state['abbreviation'] . "\n";
	$message .= 'Zip Code: ' . $_POST['zipcode'] . "\n";
	$message .= 'Email: ' . $_POST['email'] . "\n" . "\n";

	$message .= "\n" . "\n" . '-----------------------------------------------------' . "\n" . "\n";

	$message .= "Investment" . "\n";
    $message .= '** Payment Options: ' . $_POST['pay_option'] . "\n";
	$message .= '** Coaching Option: ' . $coaching_option;

	$message .= "\n" . "\n" . '-----------------------------------------------------' . "\n" . "\n";

	$message .= "Descriptions of your business (How long in business, how many members, how much monthly gross revenue do you bring in annually)" . "\n";
    $message .= '** ' . $_POST['business_description'];

	$message .= "\n" . "\n" . '-----------------------------------------------------' . "\n" . "\n";

	$message .= "What obstacles and challenges have you encountered in life and business and how did you overcome them? " . "\n";
    $message .= '** ' . $_POST['business_obstacles'];

	$message .= "\n" . "\n" . '-----------------------------------------------------' . "\n" . "\n";

	$message .= "What do you feel are the top challenges you're having in your business? " . "\n";
    $message .= '** ' . $_POST['business_challenges'];

	$message .= "\n" . "\n" . '-----------------------------------------------------' . "\n" . "\n";

	$message .= "Briefly describe why you feel you're a good candidate for the 6 Figure Mastermind Coaching Program: ";
    $message .= '** ' . $_POST['business_candidate'];

	$message .= "\n" . "\n" . '-----------------------------------------------------' . "\n" . "\n";

	$message .= "What are your business goals for the next 12 months? " . "\n";
    $message .= '** ' . $_POST['business_goals'];

    $message .= "\n" . "\n" . '-----------------------------------------------------' . "\n" . "\n";

	$message .= "Business Gross Value" . "\n" . "\n";
    $message .= '** '.(date("Y") - 1).' - ' . $_POST['year_2012'] . "\n";
	$message .= '** '.(date("Y") - 2).' - ' . $_POST['year_2011'] . "\n";
	$message .= '** '.(date("Y") - 3).' - ' . $_POST['year_2010'] . "\n";

	$message .= "\n" . "\n" . '-----------------------------------------------------' . "\n" . "\n";

	$message .= "What are your business goals for the next 12 months? " . "\n" . "\n";
    $message .= '** Clarity of Goals & Objectives: ' . $_POST['rating_clarity'] . "\n";
	$message .= '** Time Management & Personal Productivity: ' . $_POST['rating_time_mangement'] . "\n";
	$message .= '** Handling Employees/Outsourcing/Vendors: ' . $_POST['rating_outsourcing'] . "\n";
	$message .= '** Raising Prices: ' . $_POST['rating_prices'] . "\n";
	$message .= '** Customer Service: ' . $_POST['rating_customer_service'] . "\n";
	$message .= '** Dealing with increased competition: ' . $_POST['rating_competition'] . "\n";
	$message .= '** Copywriting: ' . $_POST['rating_copywriting'] . "\n";
	$message .= '** Email Strategy: ' . $_POST['rating_email_strategy'] . "\n";
	$message .= '** Program/Service development/creation: ' . $_POST['rating_creation'] . "\n";
	$message .= '** Program launch/re-launch: ' . $_POST['rating_program_launch'] . "\n";
	$message .= '** Traffic strategies: ' . $_POST['rating_traffic_strategies'] . "\n";
	$message .= '** Tracking & testing: ' . $_POST['rating_tracking_testing'] . "\n";
	$message .= '** Conversion and multiple purchases: ' . $_POST['rating_conversion'] . "\n";
	$message .= '** Affiliate and joint venture marketing: ' . $_POST['rating_affiliate'] . "\n";
	$message .= '** Offline marketing: ' . $_POST['rating_offline_marketing'] . "\n";

    $message .= "\n" . "\n" . '-----------------------------------------------------' . "\n" . "\n";

	$message .= "Self-Assessment: " . "\n" . "\n";
    $message .= '** Advertising, Marketing Skills: ' . $_POST['rating_marketing'] . "\n";
	$message .= '** Time Allocated To Marketing & Sales: ' . $_POST['rating_time_allocated'] . "\n";
	$message .= '** Systematic, On Schedule Achievement Of Goals: ' . $_POST['rating_systematic'] . "\n";
	$message .= '** Time Management & Personal Productivity: ' . $_POST['rating_personal_productivity'] . "\n";
	$message .= '** Passion, Enthusiasm, Mind Set, Enjoyment Of Business Life: ' . $_POST['rating_passion'] . "\n";
	$message .= '** Satisfaction With Income & Income Growth: ' . $_POST['rating_income_growth'] . "\n";
	$message .= '** Quality Of Personal, Family Related Relationships: ' . $_POST['rating_relationships'] . "\n";

	$message .= "\n" . "\n" . '-----------------------------------------------------' . "\n" . "\n";

	$message .= "What's important for Michael to know about you personally? " . "\n" . "\n";
    $message .= '** ' . $_POST['important_note'];

	$headers = 'From: mastermind@fconlinemarketing.com';

	mail($to, $subject, $message, $headers);

	// everything complete, redirect to success page
	header("Location: ../success.php");

else:
	$statecode = $_POST['statecode'];
	$sql = "SELECT abbreviation FROM states WHERE id = $statecode";

	$data = mysql_query($sql);

	while($row = mysql_fetch_assoc($data)):
		$state = $row;
	endwhile;

	if($_POST['coaching_option'] == ''):
		$coaching_option = 'No';
	else:
        $coaching_option = 'Yes';
	endif;

	//$to      = 'ryan@fconlinemarketing.com';
	$to      = 'james@fconlinemarketing.com';
	$subject = 'MasterMind Application';

	$message = 'Database Insertion Failed...';

	$message .= "Contact Information" . "\n" . "\n";

	$message .= 'Name: ' . ucwords($_POST['fullname']) . "\n";
	$message .= 'Phone: ' . $_POST['phone'] . "\n";
	$message .= 'Address: ' . $_POST['address'] . ' ' . $_POST['address1'] . "\n";
	$message .= 'City: ' . $_POST['city'] . "\n";
	$message .= 'State: ' . $state['abbreviation'] . "\n";
	$message .= 'Zip Code: ' . $_POST['zipcode'] . "\n";
	$message .= 'Email: ' . $_POST['email'] . "\n" . "\n";

	$message .= "\n" . "\n" . '-----------------------------------------------------' . "\n" . "\n";

	$message .= "Investment" . "\n";
    $message .= '** Payment Options: ' . $_POST['pay_option'] . "\n";
	$message .= '** Coaching Option: ' . $coaching_option;

	$message .= "\n" . "\n" . '-----------------------------------------------------' . "\n" . "\n";

	$message .= "Descriptions of your business (How long in business, how many members, how much monthly gross revenue do you bring in annually)" . "\n";
    $message .= '** ' . $_POST['business_description'];

	$message .= "\n" . "\n" . '-----------------------------------------------------' . "\n" . "\n";

	$message .= "What obstacles and challenges have you encountered in life and business and how did you overcome them? " . "\n";
    $message .= '** ' . $_POST['business_obstacles'];

	$message .= "\n" . "\n" . '-----------------------------------------------------' . "\n" . "\n";

	$message .= "What do you feel are the top challenges you're having in your business? " . "\n";
    $message .= '** ' . $_POST['business_challenges'];

	$message .= "\n" . "\n" . '-----------------------------------------------------' . "\n" . "\n";

	$message .= "Briefly describe why you feel you're a good candidate for the 6 Figure Mastermind Coaching Program: ";
    $message .= '** ' . $_POST['business_candidate'];

	$message .= "\n" . "\n" . '-----------------------------------------------------' . "\n" . "\n";

	$message .= "What are your business goals for the next 12 months? " . "\n";
    $message .= '** ' . $_POST['business_goals'];

    $message .= "\n" . "\n" . '-----------------------------------------------------' . "\n" . "\n";

	$message .= "Business Gross Value" . "\n" . "\n";
    $message .= '** '.(date("Y") - 1).' - ' . $_POST['year_2012'] . "\n";
	$message .= '** '.(date("Y") - 2).' - ' . $_POST['year_2011'] . "\n";
	$message .= '** '.(date("Y") - 3).' - ' . $_POST['year_2010'] . "\n";

	$message .= "\n" . "\n" . '-----------------------------------------------------' . "\n" . "\n";

	$message .= "What are your business goals for the next 12 months? " . "\n" . "\n";
    $message .= '** Clarity of Goals & Objectives: ' . $_POST['rating_clarity'] . "\n";
	$message .= '** Time Management & Personal Productivity: ' . $_POST['rating_time_mangement'] . "\n";
	$message .= '** Handling Employees/Outsourcing/Vendors: ' . $_POST['rating_outsourcing'] . "\n";
	$message .= '** Raising Prices: ' . $_POST['rating_prices'] . "\n";
	$message .= '** Customer Service: ' . $_POST['rating_customer_service'] . "\n";
	$message .= '** Dealing with increased competition: ' . $_POST['rating_competition'] . "\n";
	$message .= '** Copywriting: ' . $_POST['rating_copywriting'] . "\n";
	$message .= '** Email Strategy: ' . $_POST['rating_email_strategy'] . "\n";
	$message .= '** Program/Service development/creation: ' . $_POST['rating_creation'] . "\n";
	$message .= '** Program launch/re-launch: ' . $_POST['rating_program_launch'] . "\n";
	$message .= '** Traffic strategies: ' . $_POST['rating_traffic_strategies'] . "\n";
	$message .= '** Tracking & testing: ' . $_POST['rating_tracking_testing'] . "\n";
	$message .= '** Conversion and multiple purchases: ' . $_POST['rating_conversion'] . "\n";
	$message .= '** Affiliate and joint venture marketing: ' . $_POST['rating_affiliate'] . "\n";
	$message .= '** Offline marketing: ' . $_POST['rating_offline_marketing'] . "\n";

    $message .= "\n" . "\n" . '-----------------------------------------------------' . "\n" . "\n";

	$message .= "Self-Assessment: " . "\n" . "\n";
    $message .= '** Advertising, Marketing Skills: ' . $_POST['rating_marketing'] . "\n";
	$message .= '** Time Allocated To Marketing & Sales: ' . $_POST['rating_time_allocated'] . "\n";
	$message .= '** Systematic, On Schedule Achievement Of Goals: ' . $_POST['rating_systematic'] . "\n";
	$message .= '** Time Management & Personal Productivity: ' . $_POST['rating_personal_productivity'] . "\n";
	$message .= '** Passion, Enthusiasm, Mind Set, Enjoyment Of Business Life: ' . $_POST['rating_passion'] . "\n";
	$message .= '** Satisfaction With Income & Income Growth: ' . $_POST['rating_income_growth'] . "\n";
	$message .= '** Quality Of Personal, Family Related Relationships: ' . $_POST['rating_relationships'] . "\n";

	$message .= "\n" . "\n" . '-----------------------------------------------------' . "\n" . "\n";

	$message .= "What's important for Michael to know about you personally? " . "\n" . "\n";
    $message .= '** ' . $_POST['important_note'];

	$headers = 'From: scott@fconlinemarketing.com';

	mail($to, $subject, $message, $headers);

	// everything complete, redirect to success page
	header("Location: ../success.php");

endif;

dbKillConn();

?>



