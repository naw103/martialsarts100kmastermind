<?php
	include("program/program.php");
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8" />
<title>Six-Figure Martial Arts MasterMind</title>

<meta name="Description" content=""/>

<meta name="keywords" content=""/>


<link rel="stylesheet" href="https://www.ilovekickboxing.com/intl_css/reset.css"/>
<link rel="stylesheet" href="css/pages.css"/>
<link rel="stylesheet" href="css/index.css"/>

<script src="https://code.jquery.com/jquery-1.9.1.js"></script>
<!--[if lt IE 9]>
  <script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-29420623-21', 'martialarts100kmastermind.com');
  ga('send', 'pageview');

</script>


</head>
<body>
<?php include("header.php"); ?>

<div class="container">

	<!-- BEGIN: Page Content -->
	<div id="page_content">
		<?php include('navigation.php'); ?>
		<div style="padding: 0px 20px;">
	   	<p style="text-align: center; padding-top: 80px;"><strong>PRIVACY POLICY:</strong></p>

		<p>
			We respect and are committed to protecting your privacy. We may collect
			personally identifiable information when you visit our site. We also automatically
			receive and record information on our server logs from your browser including
			your IP address, cookie information and the page(s) you visited. We will not sell
			your personally identifiable information to anyone. (And so on...)
		</p>

		<p><strong>SECURITY POLICY:</strong></p>

		<p>
			Your payment and personal information is always safe. Our Secure Sockets
Layer (SSL) software is the industry standard and among the best software
available today for secure commerce transactions. It encrypts all of your personal
information, including credit card number, name, and address, so that it cannot
be read over the internet. (Etc.)
		</p>

		<p><strong>REFUND POLICY:</strong></p>

		<p>
All refunds will be provided as a credit to the credit card used at the time of
purchase within five (5) business days upon receipt of the returned
merchandise.
		</p>

			<p><strong>Shipping Policy/Delivery Policy:</strong></p>

		<p>
Please be assured that your items will ship out within two days of purchase.
We determine the most efficient shipping carrier for your order. The carriers that
may be used are: U.S. Postal Service (USPS), United Parcel Service (UPS) or
FedEx. Sorry but we cannot ship to P.O. Boxes.
<br /><br />
If you're trying to estimate when a package will be delivered, please note the
following:
Credit card authorization and verification must be received prior to processing.
Federal Express and UPS deliveries occur Monday through Friday, excluding
holidays.
If you require express or 2 day shipping, please call us at 303.477.3361 for
charges.
		</p>
		</div>
	</div>
	<!-- END: Page Content -->

</div>

<?php include("footer.php"); ?>

</body>
</html>