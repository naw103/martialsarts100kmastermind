$(document).ready(function(){

	var total = $('span#order_total');

	$('input#btn_coupon').attr('disabled', false);
	$('input[name="coupon"]').attr("readonly", false);

    
	$('input#alert_ok, #alert_overlay').click(function(){

		close_alertify();

	});

    function alertify(txt)
    {
        var overlay = $('#alert_overlay');
        var container = $('#alert_container');
        var copy = $('#alert_copy');

        overlay.height($(document).height());

         // Show POPUP, Lets Position it
		//var pos = $('#checkout').position();

		var left_pos = ($(window).width() / 2) - (container.outerWidth() / 2);

		var top_pos = ($(window).height() / 2) - (container.outerHeight() / 1.5)


		container.css({
										'left': left_pos + 'px',
										'top': top_pos + 'px',
										'position': 'fixed'
									});

        copy.html(txt);


        overlay.fadeIn('fast');
        container.fadeIn('fast');
    }

	function close_alertify()
	{
		$('#alert_overlay').hide();
		$('#alert_container').hide();
	}

	// If State DropDown is changed
	$('select[name="country"]').change(function()
	{
		var country_id = $(this).val();


		$.post(
				"ajax/get_states.php",
				{ country_id: country_id },
				function(xml)
				{
					var option = '<option value="">Please Select</option>';

					$(xml).find('response state').each(function()
					{
						var state_id = $(this).find('state_id').text();
						var state = $(this).find('state_name').text();

						option += '<option value="' + state_id + '">' + state + '</option>';

					});

					$('select[name="state"]').html(option);


				}
		);

	});

	// Checkout Upsell Container
	$('#checkout').click(function()
	{


		//if(validateForm())
		if(validateForm() && $('div#loader').is(':hidden'))
        {
			// Contact Info
			var firstname = $('input[name="firstname"]').val();
			var lastname = $('input[name="lastname"]').val();
			var address = $('input[name="address"]').val();
			var address2 = $('input[name="address2"]').val();
			var city = $('input[name="city"]').val();
			var zipcode = $('input[name="zipcode"]').val();
			var statecode = $('select[name="state"]').val();
			var countrycode = $('select[name="country"]').val();
			var phone = $('input[name="phone"]').val();
			var email = $('input[name="email"]').val();
			var mm_option = $('input[name="mm_when"]:checked').val();


			// Credit Card Info
			var cc_name = $('input[name="cc_name"]').val();
			var cc_num = $('input[name="cc_num"]').val();
			var cc_type = '';

			$('ul.cards').find('li').each(function()
			{
				if(!$(this).hasClass('off'))
				{
					cc_type = $(this).attr('class');
				}

			});

			var cc_exp_month = $('select[name="cc_exp_month"]').val();
			var cc_exp_year = $('select[name="cc_exp_year"]').val();
			var cc_code = $('input[name="cvv2"]').val();
            var coupon_code = $('input[name="coupon"]').val();

            $('div#overlay').height($(document).height());

             // Show POPUP, Lets Position it
    		var pos = $('#checkout').position();

    		var left_pos = ($(window).width() / 2) - ($('#loader').outerWidth() / 2);

    		var top_pos = ($(window).height() / 2) - ($('#loader').outerHeight() / 1.5)


    		$('#loader').css({
    										'left': left_pos + 'px',
    										'top': top_pos + 'px',
    										'position': 'fixed'
    									});


            $('div#overlay').fadeIn('fast');
			$('div#loader').fadeIn('fast');

			$.ajax({
					url: 'ajax/process_order.php',
					type: 'POST',
					dataType: 'xml',
					data:
					{
						firstname: firstname,
						lastname: lastname,
						address: address,
						address2: address2,
						city: city,
						zipcode: zipcode,
						statecode: statecode,
						countrycode: countrycode,
						phone: phone,
						email: email,
						mm_option: mm_option,

						cc_name: cc_name,
						cc_num: cc_num,
						cc_type: cc_type,
						cc_exp_month: cc_exp_month,
						cc_exp_year: cc_exp_year,
						cc_code: cc_code,
                        coupon_code: coupon_code
					},
					complete: function(xml, status)
					{
                       notify_user(xml.responseText);
					}

				});

		}
		else
		{

			// This will shoot me an email with any issues that may arrive here.
			$.ajax({
					url: 'ajax/error_validate.php',
					type: 'POST',
					dataType: 'xml',
					data:
					{
						subject: 'Register Button Click',
						firstname: $('input[name="firstname"]').val(),
						lastname: $('input[name="lastname"]').val(),
						address: $('input[name="address"]').val(),
						address2: $('input[name="address2"]').val(),
						city: $('input[name="city"]').val(),
						zipcode: $('input[name="zipcode"]').val(),
						statecode: $('input[name="statecode"]').val(),
						countrycode: $('input[name="countrycode"]').val(),
						phone: $('input[name="phone"]').val(),
						email: $('input[name="email"]').val(),
						mm_option: $('input[name="mm_when"]:checked').val(),


						cc_name: $('input[name="cc_name"]').val(),
	                    coupon_code: $('input[name="coupon"]').val(),
						errors: $('.error').length
					}
			});

		}

	});


    function notify_user(xml)
    {

        var approved = 0;
        var valid_card = 0;
        var authorization_code = '';
        var transaction_id = '';
        var response_reason = '';
        var invoice_num = '';

        $(xml).find('result').each(function()
        {
            approved = Number($(this).find('approved').text());
            valid_card = Number($(this).find('valid_card').text());
            authorization_code = $(this).find('authorization_code').text();
            transaction_id = $(this).find('transaction_id').text();
            response_reason = $(this).find('response_reason').text();
            invoice_num = $(this).find('invoice_num').text();
        });


        if(approved && valid_card)
        {
            window.location.href = 'thankyou.php?id=' + invoice_num;
        }
        else
        {
            $('div#loader').hide();
            $('div#overlay').hide();

            alertify(response_reason);
        }
    }

	// FORM VALIDATION
	function validateForm()
	{

	   $('#form_wrapper').find('input.required, select.required').each(function()
	   {
			if($(this).val() == '')
			{
				$(this).addClass('error');
			    $(this).removeClass('valid');

			}
			else
			{
			   if($(this).attr('name') == 'email')
			{
				// Email field has data in it
				// Validate to make sure its a valid email format
				var str = $('input[name="email"]').val();

				var check_at = str.indexOf('@');

				var check_dot = str.indexOf('.');

				if(check_at != -1 && check_dot != -1)
				{
					$(this).removeClass('error');
					$(this).addClass('valid');
			   		$(this).next('div.error_msg').remove();
				}
				else
				{
					$(this).removeClass('valid');
					$(this).addClass('error');


				}
			}
			else
			{
				$(this).removeClass('error');
				$(this).addClass('valid');

			   $(this).next('div.error_msg').remove();

		   }
			}
	   });

	   validate_radio();

       if($('.error').length > 0)
       {
            return 0;
       }

       return 1;

	}


	function validate_radio()
	{
		if(!$('input[type="radio"]').is(':checked'))
	   {
			$('#radio_wrapper').addClass('error');
			$('#radio_wrapper').removeClass('valid');
	   }
	   else
	   {
			$('#radio_wrapper').removeClass('error');
			$('#radio_wrapper').addClass('valid'); console.log('btm');
	   }
	}

	$('input[type="radio"]').click(function(){

		validate_radio();

	});


	$('input.required').keyup(function()
	{
		if($(this).val() != '')
		{
			if($(this).attr('name') == 'email')
			{
				// Email field has data in it
				// Validate to make sure its a valid email format
				validate_email();
			}
			else
			{
				$(this).removeClass('error');
				$(this).addClass('valid');

			   $(this).next('div.error_msg').remove();

		   }
		}
		else
		{
			$(this).removeClass('valid');
			$(this).addClass('error');



		}

	});


	$('input.required').blur(function()
	{
		if($(this).val() != '')
		{
			if($(this).attr('name') == 'email')
			{
				// Email field has data in it
				// Validate to make sure its a valid email format
				validate_email();
			}
			else
			{
				$(this).removeClass('error');
				$(this).addClass('valid');

			   $(this).next('div.error_msg').remove();

		   }
		}
		else
		{
			$(this).removeClass('valid');
			$(this).addClass('error');



		}

	});


	function validate_email()
	{
		var str = $('input[name="email"]').val();

		var check_at = str.indexOf('@');

		var check_dot = str.indexOf('.');

		if(check_at != -1 && check_dot != -1)
		{
			$('input[name="email"]').removeClass('error');
			$('input[name="email"]').addClass('valid');
		}
		else
		{
			$('input[name="email"]').removeClass('valid');
			$('input[name="email"]').addClass('error');
		}
	}

	$('input[name="address2"]').keyup(function()
	{
		if($(this).val() != '')
		{
			$(this).addClass('valid');
		}
		else
		{
			$(this).removeClass('valid');
		}

	});




	$('select.required').change(function()
	{
		if($(this).val() != '')
		{
			$(this).removeClass('error');
			$(this).addClass('valid');

			$(this).next('div.error_msg').remove();
		}
		else
		{
			$(this).removeClass('valid');
			$(this).addClass('error');

		}

	});


	$('span#cvv2_define').click(function() {



		var left_pos = ($(window).width() / 2) - ($('#cvv2').width() / 2);

			var top_pos = ($(window).height() / 2) - ($('#cvv2').height() / 2)

			$('#cvv2').css({
											'left': left_pos + 'px',
											'top': top_pos + 'px',
											'position': 'fixed'
										});

		$('#cvv2').show();


		$('div#cvv2_overlay').height($(document).height());
		$('div#cvv2_overlay').show();

	});


	$('input[name="firstname"], input[name="lastname"]').blur(function(){
		$('input[name="cc_name"]').val($('input[name="firstname"]').val() + ' ' + $('input[name="lastname"]').val());
	});


	function calculate_total()
	{
		// This method will return the order total,
		// factoring in coupons/discounts if applied.
		var elem = $('tr.order_total');
        var total = 0;


        total = add_ticket();
        total = apply_coupon(total);
        //console.log(total);
        // update order total
        elem.find('td:eq(1)').html(num_to_money(total));

	}


    function apply_coupon(total)
    {
        // Find if coupon is applied
		var coupon_amt = $('tr.order_total').attr('data-couponamt');

		if(coupon_amt != '')
		{
			var coupon_type = $('tr.order_total').attr('data-coupontype');

			if(coupon_type == '$')
			{
				// Amount
				total = total - coupon_amt;

                $('tr.coupon_amt').show();

			}
			else
			{
				// Percent
                var percent = coupon_amt / 100;

                var discount = total * percent;

                total = total - discount;

                $('tr.coupon_amt').find('td:eq(1)').html(num_to_money(discount));
                $('tr.coupon_amt').show();
			}
		}

        return total;
    }

    function add_ticket()
    {
         return Number($('tr.1day').attr('data-cost'));
	}


    function num_to_money(num)
    {
		if(Math.round(num) !== num) {
        	return  '$' + num;
		}
		else
		{
			return  '$' + num.toFixed(2);
		}

    }

    function num_to_percent(percent)
    {
        return  percent + '%';
    }



	$('input#btn_coupon').click(function(){
		var coupon_code = $('input[name="coupon"]').val();

		$.ajax({
				url: 'ajax/check_coupon.php',
				type: 'POST',
				dataType: 'xml',
				data:
				{
					coupon_code: coupon_code
				},
				complete: function(xml, status)
				{
					notify_coupon(xml.responseText);
				}
		});


	});


    function notify_coupon(xml)
    {
        // This method will perform the steps taken after
        // finding if the coupon is active or not.

        var status = '';
        var msg = '';
        var amt = '';
        var type = '';
        var coupon_products = '';


        $(xml).find('coupon').each(function(){

            status = $(this).find('status').text();
            msg = $(this).find('msg').text();
            amt = Number($(this).find('amount').text());
            type = $(this).find('type').text();
            coupon_products = Number($(this).find('products').text());

        });


        if(status == 0)
        {
            alert(msg);
        }
        else
        {
            console.log(type)
			$('tr.order_total').attr('data-coupontype', type);
			$('tr.order_total').attr('data-couponamt', amt);
            $('tr.order_total').attr('data-couponprod', coupon_products);

			if(type == '$')
			{
				$('tr.coupon_amt').find('td:eq(1)').html(num_to_money(amt));
				$('tr.coupon_amt').show();
			}
			else
			{
                $('tr.coupon_discount').find('td:eq(1)').html(num_to_percent(amt));
                $('tr.coupon_discount').show();
			}

			calculate_total();

			$('input#btn_coupon').attr('disabled', true);
			$('input[name="coupon"]').attr("readonly", true);
        }

    }

});