<?php
date_default_timezone_set('America/New_York');

$order_date = date('Y-m-d H:i:s');

$product_id = 8;

$today = date('Y-m-d');

// Check for expired credit card before monthly payments end
$valid_card = FALSE;
$approved = FALSE;
$authorization_code = '';
$transaction_id = '';
$response_reason = '';
$invoice_num = '';


if(isset($_POST))
{
	# Get all data
	foreach($_POST as $key => $value):

		$$key = ucwords(trim($value));

	endforeach;



	# Load database class
    require('../program/definitions.php');
	require('../program/class.checkout.php');
	require('../program/functions.php');

	$db = new Checkout(DB_HOST, DB_USER, DB_PASS, DB_NAME);
	$db->open();


	# We must check to see if user is a 1st time customer
	$email = trim(strtolower($email));

	$sql = "SELECT
						*
			FROM
						checkout_orders co,
						checkout_customers cc
			WHERE
						cc.customer_id = co.customer_id AND
						cc.email = '$email' AND
						co.order_product_id LIKE '1DAYMM-%'";


	$db->Execute($sql);

	$unique_visitor = $db->getNumRows();

	if($unique_visitor == 0):

		$sql = "SELECT
							*
				FROM
							checkout_orders co,
							checkout_customers cc
				WHERE
							cc.customer_id = co.customer_id AND
							cc.firstname LIKE '$firstname' AND
							cc.lastname LIKE '$lastname' AND
							cc.zipcode LIKE '$zipcode' AND
							co.order_product_id LIKE '1DAYMM-%'";


		$db->Execute($sql);

		$unique_visitor = $db->getNumRows();

	endif;
	
	if($unique_visitor == 0 || TRUE)
	{

		# Get prices from database
		$sql = "SELECT product_code, cost
				FROM checkout_products
				WHERE product_id = $product_id";

		$result = $db->Execute($sql);

		// cost & product code
		$product_cost = $result['cost'];
		$product_code = $result['product_code'];

	    // calculate order total based on prices from database
		$order_total = $product_cost;

	    # Check coupon
	    $sql = "SELECT
							*
				  FROM
				  			checkout_coupons cc
				  WHERE
				  			DATE(start_date) <= DATE('$today') AND
							DATE(end_date) >= DATE('$today') AND
							coupon_code = '$coupon_code' AND
							active = 1";


		$result = $db->Execute($sql);

	    $coupon = array();
		if($db->getNumRows() > 0):
	        // active coupon
	        $coupon['code'] = $result['coupon_code'];
	        $coupon['amount'] = $result['amount'];
	        $coupon['type'] = $result['type'];

	        # apply coupon here
	        if($coupon['type'] == '$')
	    	{
	    		// Amount
	    		$order_total = $order_total - $coupon['amount'];
	    	}
	        else
	    	{
	    		// Percent
	            $percent = $coupon['amount'] / 100;

	            $discount = $order_total * $percent;

	            $order_total = $order_total - $discount;
	    	}


	    endif;


		# get state name
		$state = $db->get_state($statecode);

		# get country name
		$country = $db->get_country($countrycode);;

		/*
		 *  Use AuthorizeNet API to post payment
		 *  if approved
		 		- insert record into database
		 		- email customer   	 *
		*/



	  	if(date('Y-m', strtotime($order_date)) <= date('Y-m', strtotime($cc_exp_year . '-' . $cc_exp_month))):
	  	    $valid_card = TRUE;
	  	else:
	  		$response_reason = 'Credit Card is Expired';
	  	endif;


	  	if($valid_card)
	  	{

	        # connect to authorize.net
	  	    require('../anet_php_sdk/AuthorizeNet.php'); // The SDK

	        $sale = new AuthorizeNetAIM;
	        $sale->setSandbox(AUTHORIZENET_SANDBOX);
	        $sale->test_request = TEST_REQUEST;

	        $sale->setFields(
	                array(
	        	        'amount' => $order_total,
	                        'card_num' => $cc_num,
	                        'card_code' => $cc_code,
	                        'exp_date' => $cc_exp_month . "/" . $cc_exp_year,
	                        'first_name' => $firstname,
	                        'last_name' => $lastname,
	                        'address' => $address . " " . $address2,
	                        'city' => $city,
	                        'state' => $state,
	                        'country' => $country,
	                        'zip' => $zipcode,
	                        'phone' => $phone,
	                        'email' => $email,
	        				'invoice_num' => '1DAYMM',
	        				'description' => '1 Day MasterMind'
	        	)
	        );


	        // Aurthorize payment
	        $response = $sale->authorizeAndCapture();




	        $approved = $response->approved;
			$authorization_code = $response->authorization_code;
		    $transaction_id = $response->transaction_id;
	        $response_reason = $response->response_reason_text;

	  		// check payment response
	  		if ($approved)
	  		{


	            # Create Customer Record
	          	$sql = "INSERT INTO checkout_customers (
												  firstname, lastname, address, address2, city,
												  zipcode, state_id, country_id, phone, email
				 ) VALUES (
												  '%s', '%s', '%s', '%s', '%s',
												  '%s', '%s', '%s', '%s', '%s'
				 )";

	        	$sql = sprintf($sql,
	        							mysql_real_escape_string($firstname),
	        							mysql_real_escape_string($lastname),
	        							mysql_real_escape_string($address),
	        							mysql_real_escape_string($address2),
	        							mysql_real_escape_string($city),

	        							mysql_real_escape_string($zipcode),
	        						    mysql_real_escape_string($statecode),
	        							mysql_real_escape_string($countrycode),
	        							mysql_real_escape_string($phone),
	        							mysql_real_escape_string($email)
	        					);


	        	$db->Execute($sql);

	        	// Get customer_id of newly inserted row
	        	$customer_id = mysql_insert_id();

	            # Create Order Record


	            // mask cc number
	            if($cc_num != ''):
				    $cc_num = substr($cc_num, 0, 4) . str_repeat("X", strlen($cc_num) - 8) . substr($cc_num, -4);
	            endif;

				// get customers ip address
				$ip_address = $_SERVER['REMOTE_ADDR'];

				$sql = "INSERT INTO checkout_orders (
														customer_id, subscription_type_id, cc_name, cc_num, cc_type,
														cc_exp_month, cc_exp_year, transaction_id, order_total, order_date,
														ip_address, coupon_code, hear_about_us, mm_option
						  ) VALUES (
						  								'%s', '%s', '%s', '%s', '%s',
														'%s', '%s', '%s', '%s', '%s',
														'%s','%s', '%s', '%s'
						  )";

				$sql = sprintf($sql,
	                                    mysql_real_escape_string($customer_id),
	                                    mysql_real_escape_string('1'),
										mysql_real_escape_string($cc_name),
										mysql_real_escape_string($cc_num),
										mysql_real_escape_string($cc_type),

										mysql_real_escape_string($cc_exp_month),
										mysql_real_escape_string($cc_exp_year),
	                                    mysql_real_escape_string($transaction_id),
	                                    mysql_real_escape_string($order_total),
	                                    mysql_real_escape_string($order_date),

										mysql_real_escape_string($ip_address),
										mysql_real_escape_string($coupon['code']),
										mysql_real_escape_string(''),
	                                    mysql_real_escape_string($mm_option)
								);

				$db->Execute($sql);

				// Get order id of newly inserted row
				$order_id = mysql_insert_id();


				// Create unique order id #
				$invoice_num = $product_code . '-' . $order_id;

				$sql = "UPDATE checkout_orders
						  SET order_product_id = '$invoice_num'
						  WHERE order_id = $order_id";

				$db->Execute($sql);


				# PRODUCTS JOIN TABLE
				$sql = "INSERT INTO checkout_products_join (
																product_id, order_id
						  ) VALUES (
						  										$product_id, $order_id
						  )";

				$db->Execute($sql);


	            # Send Email
	            // This is where we would send our receipt email.
	            // We are gonna handle this using a chron job.


	  		}






	  	}
}
else
{

	$valid_card = FALSE;
	$approved = FALSE;
	$authorization_code = '';
	$transaction_id = '';
	$response_reason = 'Only allowed to purchase 1 ticket. If you are receiving this by mistake, Contact (516) 543-0041.';
	$invoice_num = '';

}

$db->close();

if($approved == 0):

	$to = 'printsupport@fconlinemarketing.com';
	$subject = 'Checkout Error Validator';
	$headers = 'From: "FC Online Marketing" <info@fconlinemarketing.com>' . "\r\n" .
    		   'Reply-To: info@fconlinemarketing.com' . "\r\n" .
        	   'X-Mailer: PHP/' . phpversion();

	$message = "Approved: Failed\n";
	$message .= "Invoice: {$invoice_num}\n";
	$message .= "Valid Card: {$valid_card}\n";
	$message .= "Response: {$response_reason}\n\n";

	$message .= "Name: {$firstname} {$lastname}\n";
	$message .= "Email: {$email}\n";
	$message .= "Phone: {$phone}\n";

	mail($to, $subject, $message, $headers);

endif;

header('Content-type: text/xml');
header('Cache-control: no-cache');
echo "<?xml version=\"1.0\" ?>\n";
echo "<response>\n";

echo "\t<result>\n";

echo "\t\t<approved>" . $approved . "</approved>\n";
echo "\t\t<invoice_num>" . $invoice_num . "</invoice_num>\n";
echo "\t\t<valid_card>" . $valid_card . "</valid_card>\n";
echo "\t\t<authorization_code>" . $authorization_code . "</authorization_code>\n";
echo "\t\t<transaction_id>" . $transaction_id . "</transaction_id>\n";
echo "\t\t<response_reason>" . $response_reason . "</response_reason>\n";

echo "\t</result>\n";

echo "</response>";



}

?>