<?php
    if($_GET['id'] == ''):
        header('Location: index.php');
    endif;

	require('program/definitions.php');
	require('program/class.checkout.php');
	require('program/functions.php');

	$db = new Checkout(DB_HOST, DB_USER, DB_PASS, DB_NAME);
	$db->open();

    $id = $_GET['id'];

    $order = $db->order_details($id);

	if((int)$order['subscription_type_id'] == 7):
				$order_total = '$' . number_format($order['order_total'], 2) . ' (3 Monthly Payments of $99)';
			else:
            	$order_total = '$' . number_format($order['order_total'], 2);
			endif;

	$db->close();
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8" />
<title>1-Day Private Mastermind with Mike Parrella</title>

<meta name="description" content="Limited time offer! Experience a private, 1-day mastermind with Mike Parrella - and get ready to take your school to insane heights." />

<meta property="og:title" content="1-Day Private Mastermind with Mike Parrella" />
<meta property="og:type" content="article" />

<meta property="og:url" content="<?php echo urldecode('https://www.martialarts100kmastermind.com/1day/'); ?>" />
<meta property="og:description" content="Limited time offer! Experience a private, 1-day mastermind with Mike Parrella - and get ready to take your school to insane heights." />
<meta property="og:site_name" content="MartialArts100kMasterMind.com" />
<meta property="og:image" content="https://www.martialarts100kmastermind.com/images/index/sunset.png" />
<meta property="og:image" content="https://www.martialarts100kmastermind.com/images/index/michae-parrella.png" />
<meta property="og:image" content="https://www.martialarts100kmastermind.com/images/index/chess-piece.png" />


<link rel="stylesheet" href="https://www.ilovekickboxing.com/intl_css/reset.css"/>
<link rel="stylesheet" href="../css/pages.css"/>
<link rel="stylesheet" href="../css/index.css"/>
<link rel="stylesheet" href="css/signup.css" />


<script src="https://www.ilovekickboxing.com/intl_js/jquery.js"></script>
<!--[if lt IE 9]>
  <script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-29420623-21', 'martialarts100kmastermind.com');
  ga('send', 'pageview');

</script>


<style>

#page_content p {
  font-size: 15px;
  margin: auto 135px 15px; }

table {
  width: 680px;
  font-size: 15px;
  margin: 45px 135px 0px;
}



    table thead {
      font-size: 18px;
      background: #DDDDDD; }

	  table thead td { padding: 7px 5px; }

</style>

</head>
<body>
<?php include("../header.php"); ?>

<div class="container">

	<!-- BEGIN: Page Content -->
	<div id="page_content">

		<div id="headline_container"></div>

		    <h1 class="futura" style="text-align: center; font-size: 40px; margin-bottom: 30px;">Thank you for your purchase.</h1>

            <p>
                Hey <?php echo stripslashes($order['firstname']); ?>,
            </p>

            <p>
                We are excited for you to be joining us for our 1 Day MasterMind Event. You will receive
                an email with receipt of your purchase to <?php echo stripslashes($order['email']); ?>.
            </p>

            <p>
                Ryan or myself will reach out to your shortly with any other details that you
                will need to know before this event.
            </p>

            <p>
                If you have any other questions, don't hesistate to reach out to us yourself.
                Call (516) 543-0041.
            </p>

            <table>
                <thead>
					<tr>
						<td>Customer Details</td>
						<td>Order Details</td>
					</tr>
				</thead>

				<tbody>
					<tr>
						<td>
			                Name: <?php echo stripslashes($order['firstname']); ?> <?php echo stripslashes($order['lastname']); ?><br />
			                Address: <?php echo stripslashes($order['address']); ?><br />
			                City: <?php echo stripslashes($order['city']); ?><br />
			                State: <?php echo stripslashes($order['Code']); ?><br />
			                Zip: <?php echo stripslashes($order['zipcode']); ?><br />
			                Country: <?php echo stripslashes($order['CountryCode']); ?><br />
							Phone: <?php echo stripslashes($order['phone']); ?>
						</td>

						<td>
							Location: <?php echo stripslashes($order['Date']); ?><br />
			                Invoice #: <?php echo stripslashes($order['order_product_id']); ?><br />
			                CC: <?php echo stripslashes($order['cc_num']); ?><br />
			                CC Exp: <?php echo stripslashes($order['cc_exp_month']); ?>/<?php echo stripslashes($order['cc_exp_year']); ?><br />
			                CC Type: <?php echo stripslashes($order['cc_type']); ?><br />
							<?php if($order['coupon_code'] != ''): ?>Coupon: <?php echo stripslashes($order['coupon_code']); ?><br /><?php endif; ?>
			                Total: <?php echo $order_total; ?>
						</td>
					</tr>

				</tbody>
            </table>

	</div>
	<!-- END: Page Content -->

</div>

<?php include("footer.php"); ?>

</body>
</html>