<?php
	require('program/program.php');

	date_default_timezone_set('America/New_York');

	$todays_date = date('Y-m-d');


?>
<!DOCTYPE html>
<html>
<head>
	<title>Martial Arts Business Summitt Admin</title>

	<link rel="stylesheet" href="https://www.ilovekickboxing.com/intl_css/reset.css?ver=1.0" />
	<link rel="stylesheet" href="../css/pages.css?ver=1.0" />
	<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css" />
	<link rel="stylesheet" href="css/admin.css?ver=1.0" />

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>


</head>
<body>

<!-- BEGIN: Page Content -->
<div id="container">
	<div id="page_content">

		<?php include('header.php'); ?>

		<div id="opening_content"></div>

		<br /><br />

		<div id="featured_numbers">

			 <!-- ticket sales -->
			<div id="ticket_sales" class="featured">
				<span class="head">Tickets</span>
				<span class="num" style="display: block; padding-top: 8px; padding-bottom: 8px;"><?php echo '$' . number_format($mabs15_totals['ticket_total'], 2); ?></span>
				<span style="font-size: 18px; display: block;">(<?php echo sizeof($mabs15); ?>)</span>
			</div>

			<span style="padding-top: 79px; vertical-align: middle;">+</span>

			<!-- friends sales -->
			<div id="mastermindsales" class="featured">
				<span class="head" style="font-size: 15px;">Friends</span>
				<span class="num" style="display: block; padding-top: 8px; padding-bottom: 8px;"><?php echo '$' . number_format($mabs15_totals['guest_total'], 2); ?></span>
				<span style="font-size: 18px; display: block;">(<?php echo sizeof($friends); ?>)</span>
			</div>

			<span style="padding-top: 79px;  vertical-align: middle;">+</span>

			<!-- dvd sales -->
			<div id="dvd_sales" class="featured">
				<span class="head">DVDs</span>
				<span class="num"  style="display: block; padding-top: 8px; padding-bottom: 8px;"><?php echo '$' . number_format($mabs15_totals['dvd_total'], 2); ?></span>
				<span style="font-size: 18px; display: block;">(<?php echo sizeof($mabs15_dvd); ?>)</span>
			</div>



			<span style="padding-top: 79px;  vertical-align: middle;">=</span>

			<!-- overall total -->
			<div id="total_monies" class="featured">
				<span class="head">Total</span>
				<span class="num"  style="display: block; padding-top: 8px; padding-bottom: 8px;"><?php echo '$' . number_format(($mabs15_totals['ticket_total'] + $mabs15_totals['guest_total'] + $mabs15_totals['dvd_total']), 2); ?></span>
				<span style="font-size: 18px; display: block;"> </span>
			</div>




			<!-- # orders today -->
			<div id="todays_orders" class="featured">
				<span class="head">Today's Orders</span><br />
				<span class="num"><?php echo sizeof($orders_today); ?></span>
			</div>

			<br /><br />

			<!-- ticket sales -->
			<div id="ticket_sales" class="featured" style="visibility: hidden;">
				<span class="head">Ticket Sales</span>
				<span class="num" style="display: block; padding-top: 8px; padding-bottom: 8px;"><?php echo '$' . number_format($mabs15_totals['ticket_total'], 2); ?></span>
				<span style="font-size: 18px; display: block;">(<?php echo sizeof($mabs15); ?>)</span>
			</div>



			<!-- friends sales -->
			<div id="mastermindsales" class="featured" style="visibility: hidden;">
				<span class="head" style="font-size: 15px;">Friends</span>
				<span class="num" style="display: block; padding-top: 8px; padding-bottom: 8px;"><?php echo '$' . number_format($mabs15_totals['guest_total'], 2); ?></span>
				<span style="font-size: 18px; display: block;">(<?php echo sizeof($friends); ?>)</span>
			</div>



			<!-- dvd sales -->
			<div id="dvd_sales" class="featured" style="visibility: hidden;">
				<span class="head">DVD Sales</span><br />
				<span class="num"><?php echo '$' . number_format($mabs15_totals['dvd_total'], 2); ?></span>
			</div>





			<!-- overall total -->
			<div id="total_monies" class="featured" style="visibility: hidden;">
				<span class="head">Overall Total</span><br />
				<span class="num"><?php echo '$' . number_format(($mabs15_totals['ticket_total'] + $mabs15_totals['guest_total'] + $mabs15_totals['dvd_total']) + (sizeof($mastermind) * 297)); ?></span>
			</div>



			<!-- mabs dvd -->
			<div id="mastermind" class="featured" style="background: #DDDDDD; color: #000; margin-left: 194px; cursor: pointer;">
				<span class="head">MABS14 DVD</span>
				<span class="num"  style="display: block; padding-top: 8px; padding-bottom: 8px;"><?php echo '$' . number_format((sizeof($dvd) * 197), 2); ?></span>
				<span style="font-size: 18px; display: block;">(<?php echo sizeof($dvd); ?>)</span>
			</div>

		</div>


		<h2>All Orders</h2>

		<table class="main">
			<thead>
				<tr class="head">
					<td>Order Id</td>
					<td>Name</td>
					<td>Product<br />Code</td>
					<td>Products<br />Purchased</td>
					<td>Total</td>
					<td>
						Order<br />
						Date
					</td>
				</tr>
			</thead>

			<tbody>

	   			<?php
					$max_orderid = 0;
					foreach($orders as $order):
						if($max_orderid < $order['order_id']):
							$max_orderid = $order['order_id'];
						endif;
				?>

				<tr>
					<td><?php echo $order['order_id']; ?></td>
					<td><?php echo ucwords($order['firstname']) . ' ' . ucwords($order['lastname']); ?></td>
					<td class="code">
						<?php if(strpos($order['order_product_id'], 'MABS15') !== false): ?>
						MABS15
						<?php else: ?>
						MABSDVD
						<?php endif; ?>

					</td>

					<td>
						<?php if(strpos($order['order_product_id'], 'MABS15') !== false): ?>
						<i class="fa fa-ticket"></i>

						<?php
							for($i = 0; $i < sizeof($mabs15_dvd); $i++):
								if($order['order_id'] == $mabs15_dvd[$i]['order_id']):
									echo '<i class="fa fa-file-video-o"></i>';
									break;
								endif;
							endfor;

							for($i = 0; $i < sizeof($friends); $i++):
								if($order['order_id'] == $friends[$i]['order_id']):
									echo '<i class="fa fa-user"></i>';
									break;
								endif;
							endfor;
						?>


						<?php else: ?>
						<i class="fa fa-file-video-o"></i>
						<?php endif; ?>
					</td>

					<td>
						<?php if($order['subscription_type_id'] != 1): ?>

							<?php
								foreach($ezpay as $option):
									if($order['ez_pay'] == $option['id']):
											echo '$' . number_format(round(($order['order_total'] * $option['num'])), 2);
										break;
									endif;
								endforeach;
							?>

						<?php else: ?>
						<?php echo '$' . number_format($order['order_total'], 2); ?>
						<?php endif; ?>
					</td>
					<td><?php echo date('m/d/y', strtotime($order['order_date'])); ?></td>


				</tr>

				<?php endforeach; ?>
		</tbody>

		</table>



	</div>
</div>


<script src="js/jquery.dataTables.js"></script>


<script>
	$(document).ready(function() {

	    $('table.main').dataTable({

			    "aaSorting": [[ 0, "desc" ]],
				"aoColumnDefs": [ { "sClass": "hidden", "aTargets": [ 0 ] } ],
				"iDisplayLength": 25,
				"aLengthMenu": [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, "All"]]

		});


	} );




</script>

<script>
var max_orderid = "<?php echo $max_orderid; ?>";

(function worker() {

  $.ajax({

    url: 'ajax/update_info.php',
	type: 'POST',
	dataType: 'xml',
	data:
	{
		orderid: max_orderid
	},

    success: function(xml) {


    },

    complete: function() {

      // Schedule the next request when the current one's complete

      setTimeout(worker, 5000);

    }

  });

})();
</script>

</body>
</html>