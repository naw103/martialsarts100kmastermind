<?php

class Reports extends database
{

	public function get_total_signups()
	{
		# This method will return all signups that have happened
		# for the 1 day mastermind regardless of what date
		$data = array();

		$sql = "SELECT
							co.*,
							cc.*,
							mm.Date AS mm_text
				FROM
							checkout_orders co,
							checkout_customers cc,
							mm_1day_options mm
				WHERE
							co.mm_option = mm.ID AND
							co.customer_id = cc.customer_id AND
							co.order_product_id LIKE '1DAYMM-%'";

		$data = $this->Execute($sql);


		return $data;
	}

	public function get_event_signups()
	{
		# This method will return all signups that have happened
		# for the 1 day mastermind regardless of what date
		$data = array();
		$options = array();

		# Lets get all active options 1st
		$sql = "SELECT
								mm.ID
				FROM
								mm_1day_options mm
				WHERE
								mm.status = 1";

		$options = $this->Execute($sql);


		foreach($options as $key => $value):

			$sql = "SELECT
								co.*,
								cc.*,
								mm.Date AS mm_text
					FROM
								checkout_orders co,
								checkout_customers cc,
								mm_1day_options mm
					WHERE
								co.mm_option = mm.ID AND
								co.customer_id = cc.customer_id AND
								mm.ID = '$value' AND
								co.order_product_id LIKE '1DAYMM-%'";

			$data[$value] = $this->Execute($sql);

		endforeach;

		
		return $data;
	}
}

?>