$(document).ready(function(){

	$('#btn_coupon_submit').click(function(){

		if(validate_form())
		{

			$.ajax({
					url: 'ajax/process_coupon.php',
					type: 'POST',
					dataType: 'xml',
					data:
					{
						name: $('input[name="coupon_name"]').val(),
						code: $('input[name="coupon_code"]').val(),
						type: $('input[name="coupon_type"]').val(),
						amount: $('input[name="coupon_amt"]').val(),
						start_date: $('input[name="coupon_start"]').val(),
						end_date: $('input[name="coupon_end"]').val(),
						active: $('input[name="coupon_active"]').val(),
						action: 'insert'
					},
					complete: function(xml, status)
					{

					}
			});
		}

	});


	function validate_form()
	{
		// This method will check all coupon fields and validate if everything
		// is filled out correctly. Upon completion will return TRUE or FALSE
		var elem = $('#coupon_container');
		var elem_start = $('input[name="coupon_start"]');
		var elem_end = $('input[name="coupon_end"]');

		// check our text fields
		elem.find('input[type="text"]').each(function(){

			if($(this).val() == '')
			{
				$(this).addClass('error');
				$(this).removeClass('valid');
			}
			else
			{
				$(this).removeClass('error');
				$(this).addClass('valid');
			}

		});

		// check our selects
		elem.find('select').each(function(){

			if($(this).val() == '')
			{
				$(this).addClass('error');
				$(this).removeClass('valid');
			}
			else
			{
				$(this).removeClass('error');
				$(this).addClass('valid');
			}

		});


		// check our start & end dates
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1; //January is 0!
		var yyyy = today.getFullYear();

		var today = new Date(mm + '/' + dd + '/' + yyyy).getTime();

		var start_date = new Date($('input[name="coupon_start"]').val()).getTime();

		var end_date = new Date($('input[name="coupon_end"]').val()).getTime();


		if(end_date < today)
		{
			console.log('End Date is Invalid');
			elem_end.addClass('error');
			elem_end.removeClass('valid');
		}
		else
		{
			elem_end.removeClass('error');
			elem_end.addClass('valid');
		}

		if(start_date > end_date)
		{
			console.log('Start Date can not be greater than End Date');
			elem_start.addClass('error');
			elem_start.removeClass('valid');
		}
		else
		{
			elem_start.removeClass('error');
			elem_start.addClass('valid');
		}







		if(elem.find('.error').length > 0)
		{
			return false;
		}

		return true;
	}

});