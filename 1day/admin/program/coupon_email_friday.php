<?php



$to      = $email;
//$to = 'james@fconlinemarketing.com';

$subject = "LAST DAY! (MABS Specials)";

$message = "Hey," . "\n\n";

$message .= "At midnight tonight (PST), these deals" . "\n"
		 . "expire." . "\n\n";

$message .= "I hope you take advantage of them," . "\n"
		 .  "because I believe in our services" . "\n"
		 .  "100%." . "\n\n";

$message .= "And I've seen the amazing results" . "\n"
		 .  "they're capable of when you put" . "\n"
		 .  "them to good use." . "\n\n";

$message .= "If you have any questions about" . "\n"
		 .  "any of the products / services below," . "\n"
		 .  "reach out to my staff at (516) 543-" . "\n"
		 .  "0041." . "\n\n";

$message .= "You'll speak with a familiar face you" . "\n"
		 .  "met at MABS - might be Rose, or" . "\n"
		 .  "Tiana, or Ryan." . "\n\n";

$message .= "Here's how you can grab these" . "\n"
		 .  "specials NOW before they expire" . "\n"
		 .  "tonight:" . "\n\n";


$message .= '///////' . "\n\n";

$message .= '- WEBSITE:' . "\n\n";

$message .= 'Down Payment: $99 (Regular $599)' . "\n\n";

$message .= '- Use this link:' . "\n";

$message .= 'http://bit.ly/1j5LRdS' . "\n\n";

$message .= '- Use this coupon code:' . "\n";

$message .= 'MABSWEBSITE' . "\n\n";

$message .= '///////' . "\n\n";

$message .= '- EMAIL MACHINE:' . "\n\n";

$message .= 'Set-Up Fee: $49 (Regular $99)' . "\n"
		 .  'Monthly: $79 (Regular $99)' . "\n\n";

$message .= '- Use this link:' . "\n";

$message .= 'http://bit.ly/1j5LRdS' . "\n\n";

$message .= '///////' . "\n\n";

$message .= '- MA POWER LISTING:' . "\n\n";

$message .= 'Set-Up Fee: $49 (Regular $99)' . "\n"
		 .	'Monthly: $79 (Regular $99)' . "\n\n";

$message .= '- Use this link:' . "\n";

$message .= 'http://bit.ly/1jt0f0A' . "\n\n";

$message .= '- Coupon Code:' . "\n";

$message .= 'MABSPOWERLISTING' . "\n\n";

$message .= '///////' . "\n\n";

$message .= '- MA Pro Newsletter:' . "\n\n";

$message .= 'Just $1 Down for 30 days' . "\n\n";

$message .= '- Use this link:' . "\n";

$message .= 'http://bit.ly/1liL6yS' . "\n\n";

$message .= '///////' . "\n\n";

$message .= 'REMEMBER: THESE EXPIRE TONIGHT' . "\n"
		 .  'AT MIDNIGHT (PST).' . "\n\n";

$message .= "But whether or not you feel it's time" . "\n"
		 .  "to hop on the bandwagon, just keep" . "\n"
		 .  "taking action and putting what you" . "\n"
		 .  "learned at MABS to good use." . "\n\n";

$message .= "Peace" . "\n"
		 .  "MP" . "\n\n\n\n";


//$headers = 'From: info@martialartsbusinesssummit.com';
$headers = 'From: "FC Online Marketing" <info@fconlinemarketing.com>' . "\r\n" .
        	'Reply-To: info@fconlinemarketing.com' . "\r\n" .
			'BCC: printsupport@fconlinemarketing.com' . "\r\n" .
        	'X-Mailer: PHP/' . phpversion();

if(mail($to, $subject, $message, $headers)):

  $mail_result = 1;

else:
  $mail_result = 0;
endif;

?>