<?php

/* We will process the submitted form */
$site_id = '1DAYMM';
$product_id = '8';

date_default_timezone_set('America/New_York');
$todays_date = date('Y-m-d');


# include database definitions so we do not need to include them everytime
require('definitions.php');
require('class.database.php');
require('class.orders.php');

$db = new Orders(DB_HOST, DB_USER, DB_PASS, DB_NAME);
$db->open();

$orders = $db->all_orders($site_id);

$total = 0;
$todays_orders = 0;
foreach($orders as $order):
	$total += $order['order_total'];

	if(strtotime(date('Y-m-d', strtotime($order['order_date']))) == strtotime(date('Y-m-d'))):
		$todays_orders++;
	endif;

endforeach;



$date_orders = $db->date_orders();



$db->close();

?>