<?php

	$order_id = $_POST['order_id'];
	//$order_id = 282;

	$guest = 0;
	$dvd = 0;

	 //Load database class
	require('../program/class.database.php');
	require('../program/functions.php');

	$db = new database(DB_HOST, DB_USER, DB_PASS, DB_NAME);
	$db->open();

	$sql = "SELECT cc.firstname, cc.lastname, co.order_total, cc.email, co.order_product_id
			FROM checkout_orders co, checkout_customers cc
			WHERE co.order_id = $order_id AND co.customer_id = cc.customer_id";

	$result = $db->Execute($sql);


	$email = $result['email'];
	$order_product_id = $result['order_product_id'];
	$order_total = $result['order_total'];

	$firstname = $result['firstname'];
	$lastname = $result['lastname'];

   //	$to = 'james@fconlinemarketing.com';
    $to = $email;
	$from = 'info@fconlinemarketing.com';




	$sql = "SELECT *
			FROM checkout_products_join
			WHERE order_id = $order_id";

	$result = $db->Execute($sql);


	for($i = 0; $i < sizeof($result); $i++):

		if(intval($result[$i]['product_id']) == 2):
			$dvd = 1;
		endif;

		if(intval($result[$i]['product_id']) == 3):
			$guest = 1;
		endif;

	endfor;


	# Email customer receipt
	//$to      = $email;
	$subject = "Congrats, you're in! (IMPORTANT Info Enclosed!)";

		$message = "Hey" . "\n\n";

		$message .= "You're in. You've snagged one of the\nvery limited seats to Martial Arts\nBusiness Summit 2014." . "\n\n";

		$message .= "It's going to be a blast. And you're\ngoing to walk away with your mind\nabsolutely FRIED..." . "\n\n";

		$message .= "... with powerful, school-growing\ninformation." . "\n\n";

		$message .= "Meanwhile, it's time to get your flight\ntickets and book your hotel!" . "\n\n";

		//$message .= "Here is your MABS14 Info Packet... " . "\n\n";
		$message .= "Your MABS14 Info Packet is attached to this email... " . "\n\n";

		//$message .= "https://www.martialartsbusinesssummit.com/mabs-itenerary.pdf" . "\n\n";
	   //$message .= "https://www.martialartsbusinesssummt.com" . "\n\n";

		$message .= "Here is the hotel the event is\nlocated in:" . "\n\n";

	   	$message .= "http://www.marriott.com/hotels/travel/lgaap-new-york-laguardia-airport-marriott/" . "\n\n";

		$message .= "Mention MARTIAL ARTS BUSINESS\nSUMMIT for a special rate when you\ncall." . "\n\n";

		$message .= "-------------------------------------" . "\n\n";

		$message .= "Just to remind you, the dates\nare March 28 - 30, 2014." . "\n\n";

		$message .= "If you have any questions, don't\nhesitate to reply to this email\nor call us at (516) 543-0041." . "\n\n";

		$message .= "Also, between now and then,\nthink of any burning questions\nor problems you're facing in\nyour school." . "\n\n";

		$message .= "Think of questions you'd kill\nto know the answers to." . "\n\n";

		$message .= "You'll have opportunities\nto ask some of our industry's\nbrightest, most successful\nindividuals for help." . "\n\n";

		$message .= "I HIGHLY recommend taking\nadvantage of this." . "\n\n";

		$message .= "See you there. I can't wait." . "\n\n";

		$message .= 'Peace,' . "\n";
		$message .= 'MP' . "\n\n\n";

		$message .= "-------------------------------------" . "\n\n";

		$message .= $firstname . ' ' . $lastname . ",\n\n";

		$message .= "The details of your order are:" . "\n\n";

		$message .= "Order #:" . $order_product_id . "\n\n";

		$message .= "You have been charged: $" . number_format($order_total, 2) . "\n\n";

		$message .= "Which includes\n";

		$message .= "\t\t-1x MABS Ticket\n";

		if($guest):
			$message .= "\t\t-1x MABS Guest Ticket\n";
		endif;

		if($dvd):
			$message .= "\t\t-1x MABS DVD";
		endif;

		$message .= "\n\n-------------------------------------" . "\n\n\n";



		// Define a list of FILES to send along with the e-mail. Key = File to be sent. Value = Name of file as seen in the e-mail.
		$attachments = array(
			'mabs-itenerary.pdf' => 'mabs-itenerary.pdf'
		);

		// Define any additional headers you may want to include
		$headers = array(
			'Reply-to' => $from . "\r\n" .
        	'BCC: printsupport@fconlinemarketing.com' 
		);

		$status = mailAttachments($to, $from, $subject, $message, $attachments, $headers);
		if($status === True) {
			//print 'Successfully mailed!';
			$msg = 1;
		} else {
			//print 'Unable to send e-mail.';
			$msg = 0;
		}





	# Build XML
	header('Content-type: text/xml');
	echo "<?xml version=\"1.0\" ?>\n";
	echo "<response>\n";

		echo "\t<message>" . $msg . "</message>\n";

	echo "</response>";


?>