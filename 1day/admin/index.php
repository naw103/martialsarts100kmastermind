<?php

	require('program/program.php');

	date_default_timezone_set('America/New_York');

	$todays_date = date('Y-m-d');


?>
<!DOCTYPE html>
<html>
<head>
	<title>MasterMind 1 Day Admin</title>

	<meta name="viewport" content="intial-scale=1.0" />

	<link rel="stylesheet" href="https://www.ilovekickboxing.com/intl_css/reset.css?ver=1.0" />
	<link rel="stylesheet" href="../css/pages.css?ver=1.0" />
	<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css" />
	<link rel="stylesheet" href="css/admin.css?ver=1.0" />

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>


</head>
<body>

<!-- BEGIN: Page Content -->
<div id="container">
	<div id="page_content">

		<?php include('header.php'); ?>

		<div id="opening_content"></div>

		<br /><br />

		<div id="featured_numbers">


			<!-- # orders today -->
			<?php foreach($date_orders as $or): ?>
			<div id="todays_orders" class="featured">
				<span class="head"><?php echo $or['Date']; ?></span><br />
				<span class="num"><?php echo $or['Total']; ?></span>
			</div>

			<?php endforeach; ?>

		</div>


		<h2>All Orders</h2>

		<table class="main" style="width: 100%;">
			<thead>
				<tr class="head">
					<td>Order Id</td>
					<td>Name</td>
					<td>Email</td>
					<td>Total</td>

					<td>
						Order<br />
						Date
					</td>
					<td>
						Date
					</td>
					<!--<td>Payments</td>-->
				</tr>
			</thead>

			<tbody>

	   			<?php foreach($orders as $order): ?>
				<tr>
					<td><?php echo $order['order_id']; ?></td>
					<td><?php echo ucwords(strtolower(stripslashes($order['firstname']))) . ' ' . ucwords(strtolower(stripslashes($order['lastname']))); ?></td>

					<td><?php echo strtolower(trim($order['email'])); ?></td>

					<td><?php echo '$' . number_format($order['order_total'], 2); ?>

					<td><?php echo date('m/d/y', strtotime($order['order_date'])); ?></td>

					<td>
						<?php echo $order['Date']; ?>
					</td>

				</tr>

				<?php endforeach; ?>
		</tbody>

		</table>



	</div>
</div>


<script src="js/jquery.dataTables.js"></script>


<script>
	$(document).ready(function() {
	    oTable = $('table.main').dataTable({

			    "aaSorting": [[ 5, "desc" ], [ 4, "desc" ]],
				"aoColumnDefs": [ { "sClass": "hidden", "aTargets": [ 0 ] } ],
				"iDisplayLength": 25,
				"aLengthMenu": [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, "All"]]

		});




	} );

	$(window).resize(function(){

		 jQuery(oTable).dataTable().fnDestroy();
         oTable = $('table.main').dataTable({

			    "aaSorting": [[ 0, "desc" ]],
				"aoColumnDefs": [ { "sClass": "hidden", "aTargets": [ 0 ] } ],
				"iDisplayLength": 25,
				"aLengthMenu": [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, "All"]]

		});

		var width = $(window).width();

		if(width > 1000)
		{
			oTable.width(1000);
		}
		else
		{
			o.Table.width(width);
		}
	});




</script>

</body>
</html>