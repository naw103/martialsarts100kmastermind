<?php

	require('program/program.php');

	date_default_timezone_set('America/New_York');

	$todays_date = date('Y-m-d');


?>
<!DOCTYPE html>
<html>
<head>
	<title>Martial Arts Profits Arsenal Admin</title>

	<meta name="viewport" content="intial-scale=1.0" />

	<link rel="stylesheet" href="https://www.ilovekickboxing.com/intl_css/reset.css?ver=1.0" />
	<!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.css" rel="stylesheet">
	<link href="../css/responsive.css" rel="stylesheet">
	<link rel="stylesheet" href="../css/pages.css?ver=1.0" />
	<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css" />
	<link rel="stylesheet" href="css/admin.css?ver=1.0" />



</head>
<body>

<!-- BEGIN: Page Content -->
<div class="container">

	<div class="row">

		<div class="col-md-12">
			<div id="featured_numbers">

				<!-- ticket sales -->
				<div id="ticket_sales" class="featured">
					<span class="head">Total</span>
					<span class="num" style="display: block; padding-top: 8px; padding-bottom: 8px;"><?php echo '$' . number_format($total, 2); ?></span>
					<span style="font-size: 18px; display: block;">(<?php echo sizeof($orders); ?>)</span>
				</div>

				<!-- # orders today -->
				<div id="todays_orders" class="featured">
					<span class="head">Today's Orders</span><br />
					<span class="num"><?php echo $todays_orders; ?></span>
				</div>

			</div>
		</div>

	</div>

	<div class="row">


		<h2>All Orders</h2>

		<div class="col-md-12">
			<table class="main" style="width: 100%;">
				<thead>
					<tr class="head">
						<td>Order Id</td>
					   
						<td>Total</td>

						<td>
							Order<br />
							Date
						</td>
						<!--<td>Hear About</td>-->
					</tr>
				</thead>

				<tbody></tbody>

			</table>
		</div>

	</div>


</div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script src="../js/bootstrap.js"></script>
<script src="js/jquery.dataTables.js"></script>


<script>
	$(document).ready(function() {
	    oTable = $('table.main').dataTable({
				"bProcessing": true,
				"bServerSide": true,
				"sServerMethod": "POST",
				"sAjaxSource": "ajax/datatables.php",
			    "aaSorting": [[ 0, "desc" ]],
				"aoColumnDefs": [ { "sClass": "hidden", "aTargets": [ 0 ] } ],
				"iDisplayLength": 25,
				"aLengthMenu": [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, "All"]]

		});




	} );


</script>

</body>
</html>