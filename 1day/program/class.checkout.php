<?php

include('class.database.php');

class Checkout extends database
{

	function get_all_countries()
	{
		# This method will return all states that are eligible
		$data = array();


		$sql = "SELECT
						*
				FROM
						Countries";

		$data = $this->Execute($sql);

		return $data;
	}

	function get_all_states($country = 1)
	{
		# This method will return all states for a given country
		$data = array();

		$sql = "SELECT
							*
				FROM
							States
				WHERE
							Country = $country";

		$data = $this->Execute($sql);


		return $data;
	}

    function get_state($statecode)
	{
		# This method will return individual state for a given country based on state id
		$data = array();

		$sql = "SELECT
							*
				FROM
							States
				WHERE
							ID = 1";

		$data = $this->Execute($sql);


		return $data['State'];
	}

    function get_country($countrycode)
    {
        # This method will return individual state for a given country based on state id
		$data = array();

        $sql = "SELECT CountryCode
			FROM Countries
			WHERE ID = $countrycode";

	    $data = $this->Execute($sql);

        return $data['CountryCode'];
    }

    function order_details($invoice_num)
    {
        # This method will return info on the order purchased
		$data = array();

        $sql = "SELECT
                			cc.firstname,
                			cc.lastname,
                			cc.email,
							cc.address,
							cc.city,
							cc.zipcode,
							cc.phone,
							co.subscription_type_id,
							co.coupon_code,
                			co.order_total,
                			co.cc_type,
                            co.cc_num,
                			co.cc_exp_month,
                			co.cc_exp_year,
                			co.order_date,
                			co.order_product_id,
                			s.Code,
                			c.CountryCode,
							mm.Date,
							mm.ID AS mm_id
                FROM
                			checkout_orders co,
                			checkout_customers cc,
                			Countries c,
                			States s,
                			mm_1day_options mm
                WHERE
                			cc.customer_id = co.customer_id AND
                			cc.country_id = c.ID AND
                			cc.state_id = s.ID AND
                			co.mm_option = mm.ID AND
                			co.order_product_id = '$invoice_num'";

	    $data = $this->Execute($sql);

        return $data;
    }

	function get_dates()
	{
		# This method will return all active dates
		$data = array();

		$sql = "SELECT
							*
				FROM
							mm_1day_options
				WHERE
							status = 1";

		$data = $this->Execute($sql);

		if(!isset($data[0]))
		{
			$tmp = $data;
			$data = array();
			$data[0] = $tmp;
		}


		return $data;
	}


	function active_coupon()
	{
		$data = array();

		date_default_timezone_set('America/New_York');
		$today = date('Y-m-d');

		$sql = "SELECT
						*
			  FROM
			  			checkout_coupons cc
			  WHERE
			  			DATE(start_date) <= DATE('$today') AND
						DATE(end_date) >= DATE('$today') AND
						active = 1";

		$data = $this->Execute($sql);

		return $this->getNumRows();
	}


}


?>