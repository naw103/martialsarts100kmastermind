<!-- BEGIN: Footer -->
<footer>
	<p>All Rights Reserved &copy; Six-Figure Martial Arts Mastermind from Mike Parrella <?php echo date('Y'); ?>.</p>
</footer>
<!-- END: Footer -->

<script src="https://www.ilovekickboxing.com/intl_js/cufon.js"></script>
<script src="https://www.martialarts100kmastermind.com/js/fonts/Futura-CondensedMedium_500.font.js"></script>
<script src="https://www.martialarts100kmastermind.com/js/fonts/Daniel_400.font.js"></script>
<script src="https://www.martialarts100kmastermind.com/js/fonts/Futura_Std_300.font.js"></script>
<script src="https://www.martialarts100kmastermind.com/js/fonts/Futura_Std_oblique_500.font.js"></script>
<script src="https://www.martialarts100kmastermind.com/js/fonts/Futura_Std_Bold_Condensed.font.js"></script>
<script>
	Cufon.replace('.futura', {fontFamily: 'Futura-CondensedMedium' });
	Cufon.replace('.futura_lite', {fontFamily: 'Futura Std Lite' });
	Cufon.replace('.futura-oblique', {fontFamily: 'Futura Std' });
	Cufon.replace('.futura-bold', {fontFamily: 'Futura Std Bold' });
	Cufon.replace('.daniel', {fontFamily: 'Daniel' });
</script>
