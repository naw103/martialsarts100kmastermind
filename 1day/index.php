<?php
	require('program/definitions.php');
	require('program/class.checkout.php');
	require('program/functions.php');

	$db = new Checkout(DB_HOST, DB_USER, DB_PASS, DB_NAME);
	$db->open();

	$mm_locations = $db->get_dates();

	$db->close();
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8" />
<title>1-Day Private Mastermind with Mike Parrella</title>

<meta name="description" content="Limited time offer! Experience a private, 1-day mastermind with Mike Parrella - and get ready to take your school to insane heights." />

<meta property="og:title" content="1-Day Private Mastermind with Mike Parrella" />
<meta property="og:type" content="article" />

<meta property="og:url" content="<?php echo urldecode('https://www.martialarts100kmastermind.com/1day/'); ?>" />
<meta property="og:description" content="Limited time offer! Experience a private, 1-day mastermind with Mike Parrella - and get ready to take your school to insane heights." />
<meta property="og:site_name" content="MartialArts100kMasterMind.com" />
<meta property="og:image" content="https://www.martialarts100kmastermind.com/images/index/sunset.png" />
<meta property="og:image" content="https://www.martialarts100kmastermind.com/images/index/michae-parrella.png" />
<meta property="og:image" content="https://www.martialarts100kmastermind.com/images/index/chess-piece.png" />

<link rel="stylesheet" href="https://www.ilovekickboxing.com/intl_css/reset.css"/>
<link rel="stylesheet" href="../css/pages.css"/>
<link rel="stylesheet" href="../css/index.css"/>
<link rel="stylesheet" href="css/1day.css" />


<script src="https://www.ilovekickboxing.com/intl_js/jquery.js"></script>
<!--[if lt IE 9]>
  <script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-29420623-21', 'martialarts100kmastermind.com');
  ga('send', 'pageview');

</script>


</head>
<body>
<?php include("header.php"); ?>

<div class="container">

	<!-- BEGIN: Page Content -->
	<div id="page_content">


		<div id="headline-container">

			<h2>
				<span id="limited"><span style="background-color: #FFFF01; padding: 0px 5px;">1-Day Only Live Event!</span></span><br />
			   	<span class="futura">Come to a Private, 1-Day Mastermind<br />
				with Mike Parrella, and Get Powerful,<br />
				1-on-1 Guidance to <span class="red futura-oblique">Grow Your<br />
				School to Insane Numbers.</span></span>
			</h2>

		</div>

		<div class="testimonial-container">

			<p class="head_testimonial">Just like these guys and gals:</p>

			<div class="horiz_row" style="padding: 0; height: 1px; margin:  30px auto 0px;"></div>
			<article class="testimonial">

				<img class="headshot" src="../images/testimonials/3.png" alt="" />

				<p class="futura head">
                	"I went from not even taking a paycheck from my school to having
					2 locations that gross over $75k/month."
				</p>
				<p>
                    I started working with Michael Parella three years ago. At the time,
					I had a single location with two staff members, barely grossing
					12K/month. I was not taking a paycheck.
				</p>
				<p>
					Currently, I have two locations and seven staff members grossing
					50K/month and growing
				</p>
				<p>
					<strong>I used to work six days, 60-70 hours per week at that one
					location</strong>-I was responsible for everything. <strong>Now,
					between the two locations, I work 3-5 hours a day on site,</strong>
					reserving the rest of my work week to grow the business and spend
					quality time doing things I love.
				</p>
				<p class="daniel sig">Walter Rowe, RI</p>

			</article>

			<article class="testimonial">

				<img class="headshot" src="../images/testimonials/6.png" alt="" />

				<p class="futura head">
                	"I tried every 'expert' out there. But it was Mike Parrella who
					brought my billing from $3,000 / month to now $20,000 / month and rising."
				</p>
				<p class="daniel sig">David Meyer, MN</p>

			</article>

			<article class="testimonial" style="padding-bottom: 50px;">

				<img class="headshot" src="../images/testimonials/8-1.png" alt="" />
				<img class="headshot" src="../images/testimonials/8-2.png" alt="" />

				<p class="futura head">
                	"Monthly billing went from $2,000 to $15,000 in 60 days."
				</p>
				<p>
					We used to have 14 hour days. Now, we can actually have a life. We
					brought our kids into the business too, and we're leaving them a
					better business for their future.
				</p>
				<p>
                	From one promotion Michael taught us, In 3 days we had hundreds of
					people in our doors who we'd never seen before in our lives. Michael
					has it all down to a science.
				</p>

				<p class="daniel sig">Al & Nicole Agon, FL</p>

			</article>


			<article class="testimonial">

				<img class="headshot" src="../images/testimonials/5.png" alt="" />

				<p class="futura head">
                	"From $35k / month to $82k / month."
				</p>
				<p>
					Mike's MasterMind Group has been a game changer for my business. He
					helped me change my mindset from operating as a regular martial arts
					school owner to operating as a successful business owner.
				</p>
				<p>
					Before the MasterMind I was constantly majoring in minor things that
					brought no profitability or results to my location. Now I only focus
					on my 5% which are things that can provide results for my business
					and opportunities for my staff.
				</p>
				<p>
					Since the time we started we have experienced a change in the way we
					see and treat our students and staff. My business has also grown by
					47% and Thank God continues to grow. We are proud and thankful to be
					part of such an awesome group.
				</p>
				<p class="daniel sig">Larry Batista, NY</p>

			</article>


			<div class="signup">
				<a href="https://www.martialarts100kmastermind.com/1day/signup.php">
					&ldquo;I'm a dead-serious action<br />
					taker. Sign me up!&rdquo;
					<input class="lets_go" type="image" src="images/button.jpg" alt="Lets Go" />
				</a>
			</div>

		</div>


		<!-- BEGIN: Note -->
		<div class="mastermind-copy">
			<p class="daniel">To my fellow school owner,</p>

			<img src="images/mike-parrella.png" alt="Mike Parrella" />

        	<p>
                Hey, this is Mike Parrella, and I want to give you an awesome opportunity.
			</p>
			<p>
				See, I've been running a private mastermind group for well over a year
				now, and the results school owners are getting are simply amazing.
			</p>

			<p>
				Read a few of those testimonials above, and you'll see what I mean. And
				there are dozens more stories like that.
			</p>

			<p>
				<strong>However, my Mastermind is pricey. That's because I believe in charging
				top dollar for my valuable services.</strong>
			</p>
			<p>
				<em>So I've decided to host an exclusive, 1-day, power-packed event designed
				to give you the Mastermind experience - at a fraction of the cost.</em>
			</p>
			<p>
				Read on to get the full scoop. And if you're focused, determined, and ready
				to knock it out of the park this year - register ASAP.
			</p>
			<p>
				There are limited spaces, and limited time to hop on board.
			</p>
			<p>
				Peace,<br>
				<span class="daniel">MP</span>
			</p>
		</div>
        <!-- END: Note -->

		<article>

			<p class="check futura">The Opportunity:</p>

			<p>
                This opportunity is for a 1-day, private mastermind session with Michael
				Parrella and a small group of other school owners. You'll learn some of
				Michael's secrets that he's taught to dozens of school owners, helping
				them achieve insane results like the ones above.
			</p>
			<p>
				<strong>Michael is the ONLY guy in the martial arts industry who consistently
				generates 100+ enrollments per month in EACH of his schools...</strong>
			</p>
			<p>
				<strong><em>The crazy part? He spends no more than 2-3 hours per week
				managing all 5 of them.</em></strong>
			</p>
			<p>
				If you want the &ldquo;do what I want, when I want&rdquo; lifestyle... and
				you want to grow your school like never before... You won't want to miss this.
			</p>

			<p class="check futura">Who this is for:</p>

			<p>
                This is only for SERIOUS, ACTION-TAKER school owners who are ready to
				finally achieve amazing numbers in their schools. If you're tired of barely
				getting by, or you're doing pretty well but you're ready to absolutely kill
				it and achieve financial freedom... This is for you.
			</p>

			<p class="check futura red">When &amp; Where:</p>

			<p>
                Here are the available dates / times. Pick the ONE that works best for you
				(you can select your choice during the checkout process):<br /><br />
			</p>

			<ul>
				<?php for($i = 0; $i < sizeof($mm_locations); $i++): ?>
					<?php if((int)$mm_locations[$i]['sold_out'] == 1): ?>
						<li><span style="color: red; text-decoration: line-through;"><span style="color: #666666;"><?php echo $mm_locations[$i]['Date']; ?></span></span><span style="padding-left: 10px; color: red;">SOLD OUT</span></li>
					<?php else: ?>
						<li><?php echo $mm_locations[$i]['Date']; ?> (9am - 5pm)</li>
					<?php endif; ?>
				<?php endfor; ?>
			</ul>

			<p style="font-size: 20px;">
				<strong>NOTE! Only 15 seats available per event. Sign up now<br />to ensure you get in!</strong>
			</p>

			<p>
				Also, you may only attend ONE of these events. This is a special opportunity which we want to open up to as many people as possible.
			</p>


			<p class="check futura red">Cost:</p>

			<p>
                <span style="font-size: 20px;">Just $297, for the entire, content-packed day (plus plenty of opportunities to personally ask Mike about specific problems you face in your school).</span>
			</p>

		</article>


		<div id="the_guarantee">

			<p class="futura"><strong>The 100% Money Back Guarantee.</strong></p>

			<img id="guarantee" src="../images/index/guaranteed.png" alt="" />

			<p>
				My guarantee is simple. If you come to this 1-day, mind-blowing event... and
				feel you did NOT get your money's worth... let me or any of my staff members
				know and we'll refund you on the spot.
			</p>

			<p>
				I'm so confident in this program and the results it can help you achieve
				that it's EASY for me to offer you this guarantee.
			</p>

			<p>
				Fair enough?
			</p>

			<p>
				Then let's do this. Click the link below and let's skyrocket your school.
			</p>

			<p style="font-size: 27px;">
				<span class="daniel">MP</span>
			</p>

		</div>


		<div class="clear"></div>

		<div class="signup">
				<a href="https://www.martialarts100kmastermind.com/1day/signup.php">
					&ldquo;I'm a dead-serious action<br />
					taker. Sign me up!&rdquo;
					<input class="lets_go" type="image" src="images/button.jpg" alt="Lets Go" />
				</a>

			</div>

		<div class="testimonial-container">

			<!--<p class="head_testimonial">Just like these guys and gals:</p>-->

			<div class="horiz_row" style="padding: 0; height: 1px; margin:  30px auto 0px;"></div>
				<article class="testimonial">

				<img class="headshot" src="../images/testimonials/11.png" alt="" />

				<p class="futura head">
                	"I started the Mastermind making $24,000 / month. I now earn
					$36,000 / month while traveling the world and lovin' life."
				</p>

				<p class="daniel sig">Patrick Consing, NY</p>

			</article>

			<article class="testimonial">

				<img class="headshot" src="../images/testimonials/1-1.png" alt="" />
				<img class="headshot" src="../images/testimonials/1-2.png" alt="" />

				<p class="futura head">
                	"Our business just about doubled within 2 months of working with Mike.
					And to make all of this extra money, we now only work a total of
					4-6 hours a week."
				</p>
				<p>
                    Before joining the MasterMind our school was already rocking it.
					<strong>However, we had absolutely NO time to enjoy it.</strong> On top of
					that, we have two kids who we wanted to devote more time to (a 2 year
					old and a 4 year old).
				</p>
				<p>
					In fact, before our kids were born, Cara worked in the school right up
					until the day before she gave birth. That's how bad it was. <strong>We
					were working 6 days a week, and easily 60+ hours inside the school.</strong>
					And both of us absolutely had to be there. If one of us got sick... It
					wasn't pretty.
				</p>
				<p>
					Mike basically said to me, "Why are you making things so damn hard on
					yourself?" He was blunt, but I could tell he cared. He then told us,
					"I can show you how to do everything you're doing now WAY easier, make
					more money, and have WAY more free time so you can actually enjoy
					your lives."
				</p>
				<p>
					<strong>Today, I work 4 hours a week inside the school. Cara works 6 hours.</strong>
					We have all the time in the world for our children, our school is
					helping more people than ever with our programs, and life just
					feels good.
				</p>
				<p>
					<strong>We're both extremely grateful for MasterMind, Mike Parrella & Ryan
					Healy.</strong> They've changed our lives in the most incredible ways.
				</p>
				<p>
                	I don't know any other way to say this than to just say it: Man up
					(or woman up), make the investment, and do it. <strong>Empty your "cup"
					and just follow what Mike and Ryan have to say. It's one of the best
					decisions we've ever made.</strong>
				</p>
				<p class="daniel sig">Chuck & Kara Giangreco, NY</p>

			</article>



			<article class="testimonial">

				<img class="headshot" src="../images/testimonials/4.png" alt="" />

				<p class="futura head">
                	"I can have dinner with my family whenever I want now. And my
					monthly billing went from $25k to now doing $40k consistently"
				</p>
				<p>
                    <strong>My experience with Mike and his master mind group has been
					a life changer.</strong>
				</p>
				<p>
					Working with the Mike and the Mastermind has created more free time
					to work on the parts of my personal life and business life that were
					causing me frustration because I felt like they were not getting the
					attention they deserved.
				</p>
				<p>
					First and foremost I can have dinner with my family whenever I want
					now.  That is rare in our industry because our business hours are
					typically during that family time after school. <strong>I can go to
					my children's games, concerts and other activities and not be worried
					about what is going on in my business.</strong>
				</p>
				<p>
					Mike's advice on how to pull myself out of my business was a big
					breakthrough for me. Being out of the daily activity has <strong>allowed
					me to pursue other interests and ideas that I have been wanting to do
					for years.</strong> Having more free time has put us in a position to
					realistically expand our business at a faster and more sustainable
					rate of speed than we ever thought we could in the past.  This has
					also allowed me to break through the occasional feeling of "burnout"
					that can happen when I wasn't always feeling like I was moving forward
					or making progress with my business.
				</p>
				<p>
					I would highly recommend Mike's Mastermind group to anyone who wants
					to take themselves to and their business to an entirely new level of
					success and sustainability.
				</p>
				<p class="daniel sig">Brett Lechtenberg, UT</p>

			</article>

			<article class="testimonial">

				<img class="headshot" src="../images/testimonials/7.png" alt="" />

				<p class="futura head">
                	"I now have 3 VERY profitable locations, yet I work fewer hours than I've ever
					worked in my entire life."
				</p>

				<p class="daniel sig">David Inman, NV</p>

			</article>

			<div class="signup">
				<a href="https://www.martialarts100kmastermind.com/1day/signup.php">
					&ldquo;I'm a dead-serious action<br />
					taker. Sign me up!&rdquo;
					<input class="lets_go" type="image" src="images/button.jpg" alt="Lets Go" />
				</a>

			</div>

		</div>


		<div id="additional_copy">

			<p class="headline futura">
				If you've ever hired a martial arts biz coach before, I know what you're probably thinking...
			</p>

			<p>
				The truth is, our industry is full of some of the worst coaches known to man. Before I reached the success I have now, I hired a LOT of them - and it never got me anywhere.
			</p>

			<p>
				If you've been burned too, I'm sure you're more than hesitant about this opportunity.
			</p>

			<p>
				That's why I wanted to take a sec to tell you one MAJOR difference between me and those other guys:
			</p>

			<p>
				<strong>I actually walk the walk. I own 5 super-successful schools. Two of those are among the top-grossing in the WORLD. And using my tactics, other school owners throughout the world have achieved amazing levels of success too.</strong>
			</p>

			<p>
				I actually have proof that I am who I say I am, and that I help schools generate the results that I do. You've read some of that in the testimonials on this page.
			</p>

			<p>
				So if you can set aside your doubt for just a moment - I know this could seriously be an amazing opportunity for you. Follow your gut, and I hope you make the right call.
			</p>

			<div class="signup">
				<a href="https://www.martialarts100kmastermind.com/1day/signup.php">
					&ldquo;I'm a dead-serious action<br />
					taker. Sign me up!&rdquo;
					<input class="lets_go" type="image" src="images/button.jpg" alt="Lets Go" />
				</a>

			</div>

		</div>
	</div>
</div>

<?php include('footer.php'); ?>

</body>
</html>