<?php include("program/program.php"); ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8" />
<title>Six-Figure Martial Arts MasterMind</title>

<meta name="Description" content=""/>

<meta name="keywords" content=""/>


<link rel="stylesheet" href="https://www.ilovekickboxing.com/intl_css/reset.css"/>
<link rel="stylesheet" href="css/pages.css"/>
<link rel="stylesheet" href="css/guarantee.css"/>


<script src="https://www.ilovekickboxing.com/intl_js/jquery.js"></script>
<!--[if lt IE 9]>
  <script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-29420623-21', 'martialarts100kmastermind.com');
  ga('send', 'pageview');

</script>


</head>
<body>
<?php include("header.php"); ?>

<div class="container">

	<!-- BEGIN: Page Content -->
	<div id="page_content">
		<?php include('navigation.php'); ?>

		<div id="headline-container">

			<h1>
        		Mike Parrella's 6-Figure MasterMind. For School Owners Ready<br>
				For Rockstar  Numbers... While Working Way Less Hours.
			</h1>

			<h2 class="futura"><strong>The 100% Money Back Guarantee.</strong></h2>

		</div>

		<div class="copy">

              <img id="guarantee" src="images/index/guaranteed.png" alt="" />

			<p class="head futura">I like to keep things simple.</p>
			<p>
                There are no contracts with MasterMind. MasterMinds with<br>
				contracts are scared. They're scared that you'll discover that
				they're worthless.
			</p>
			<p>
				They're scared that you'll want your money back.
			</p>
			<p>
				<strong>I'm not worried about that. Everyone in my MasterMind<br>
				makes back their investment many times over.</strong>
			</p>
			<p>
				I believe in it so much that if you come to your first meeting...<br>
				and decide it's not right for you... I'll refund your investment
				in full.
			</p>
			<p>
				Fair enough?
			</p>
		</div>


		<a href="<?php echo $url_apply_now; ?>">
        	<img class="apply_now" src="images/index/button-apply-now.png" alt="Apply Now" />
		</a>


	</div>
	<!-- END: Page Content -->

</div>

<?php include("footer.php"); ?>
</body>
</html>